<?php

/**
 * Définit les autorisations du plugin Design system Admin
 *
 * @plugin     Design system Admin
 * @copyright  2023
 * @author     Mikaël Folio
 * @licence    GNU/GPL
 * @package    SPIP\Design_system_admin\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/*
 * Un fichier d'autorisations permet de regrouper
 * les fonctions d'autorisations de votre plugin
 */

/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function design_system_admin_autoriser() {
}


/* Exemple
function autoriser_design_system_admin_configurer_dist($faire, $type, $id, $qui, $opt) {
	// type est un objet (la plupart du temps) ou une chose.
	// autoriser('configurer', '_design_system_admin') => $type = 'design_system_admin'
	// au choix :
	return autoriser('webmestre', $type, $id, $qui, $opt); // seulement les webmestres
	return autoriser('configurer', '', $id, $qui, $opt); // seulement les administrateurs complets
	return $qui['statut'] == '0minirezo'; // seulement les administrateurs (même les restreints)
	// ...
}
*/

// -----------------
// Objet dsfrconfigurations




/**
 * Autorisation de voir le bouton d'accès rapide de création (dsfrconfiguration)
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_dsfrconfigurationcreer_menu_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('creer', 'dsfrconfiguration', '', $qui, $opt);
}

/**
* Autorisation de voir (dsfrconfigurations)
*
* @param  string $faire Action demandée
* @param  string $type  Type d'objet sur lequel appliquer l'action
* @param  int    $id    Identifiant de l'objet
* @param  array  $qui   Description de l'auteur demandant l'autorisation
* @param  array  $opt   Options de cette autorisation
* @return bool          true s'il a le droit, false sinon
**/
function autoriser_dsfrconfigurations_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
* Autorisation de voir (dsfrconfiguration)
*
* @param  string $faire Action demandée
* @param  string $type  Type d'objet sur lequel appliquer l'action
* @param  int    $id    Identifiant de l'objet
* @param  array  $qui   Description de l'auteur demandant l'autorisation
* @param  array  $opt   Options de cette autorisation
* @return bool          true s'il a le droit, false sinon
**/
function autoriser_dsfrconfiguration_voir_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation de créer (dsfrconfiguration)
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_dsfrconfiguration_creer_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], ['0minirezo', '1comite']);
}

/**
 * Autorisation de modifier (dsfrconfiguration)
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_dsfrconfiguration_modifier_dist($faire, $type, $id, $qui, $opt) {
	return in_array($qui['statut'], ['0minirezo', '1comite']);
}

/**
 * Autorisation de supprimer (dsfrconfiguration)
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_dsfrconfiguration_supprimer_dist($faire, $type, $id, $qui, $opt) {
	return $qui['statut'] == '0minirezo' and !$qui['restreint'];
}
