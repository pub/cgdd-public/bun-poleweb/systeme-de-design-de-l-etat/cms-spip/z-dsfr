<?php
    echo '<div class="aide_onglet_navigateur aide_address_email_responsable overlay">
            <ul>
                <li><h3>Aide sur le champ "Adresse email du responsable"</h3></li>
                <li>Ce champ correspond à l\'adresse email de l\'administrateur du site.</li>                
                <li>Ce champ est obligatoire et une adresse email valide est requise.</li> 
            </ul>
        </div>';
?>