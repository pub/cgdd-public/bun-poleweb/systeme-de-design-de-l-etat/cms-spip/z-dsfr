<?php
    echo '
    <ul class="masquer_liens_legaux_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer le message de licence"</h3></li>
        <li>Ce champ permet de configurer votre message de licence.</li>
        <li>C\'est un champ obligatoire, qui doit être présent à la fin de votre footer.</li>
    </ul>';
    
?>