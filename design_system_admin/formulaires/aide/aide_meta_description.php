<?php
    echo '<div class="aide_onglet_navigateur aide_meta_description overlay">
            <ul>
                <li><h3>Aide sur le champ "Meta description"</h3></li>
                <li>Ce champ permet de définir la méta "Description" dans le header. Si une de vos pages n\'a pas de méta description, celle-ci sera utilisé par défaut.</li>                
                <li>Pour les rubriques: la méta description utilise le champ "Texte Explicatif".</li>
                <li>Pour les articles: la méta description utilise le champ "Descriptif".</li>
            </ul>
            <ul>
                <li>N\'oubliez pas d\'activer ces champs si nécessaire sur la <a href="/ecrire/?exec=configurer_contenu">page de configuration</a> de SPIP.</li>                
            </ul>
        </div>';
?>