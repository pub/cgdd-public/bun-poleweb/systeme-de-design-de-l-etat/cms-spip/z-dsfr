<?php
    echo '
    <ul class="masquer_liens_ecosysteme_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer la liste de liens obligation légale</h3></li>
        <li>Vous trouverez ci-dessous, les 4 liens de références de l\'écosystème institutionnel. Il s\'agit de liens obligatoires.</li>
        <li>Vous ne pourrez pas masquer ces liens.</li>
        <li>Seuls la mise à jour des liens est autorisée.</li>
        <li><a href="'.$_GET['chemin_plugin'].'z-dsfr/design_system_admin/prive/themes/spip/images/ecosysteme.jpg" target="_blank" title="afficher l\'image - nouvelle fenêtre"><img src="'.$_GET['chemin_plugin'].'z-dsfr/design_system_admin/prive/themes/spip/images/ecosysteme.jpg" width="250" /></a><br>
        </li>
    </ul>';
    
?>