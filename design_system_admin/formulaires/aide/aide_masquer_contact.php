<?php
    echo '<div class="aide_onglet_navigateur aide_masquer_contact masquer_contact_secondaire">
            <ul>
                <li><h3>Aide sur la fonctionnalité "Masquer la page contact?"</h3></li>
                <li>Cette fonctionnalité ajoute des paramètres supplémentaires à l\'affichage du formulaire de contact par défaut.</li>                
                <li>Les paramètres supplémentaires sont les suivants :
                    <ul>
                        <li>- Autoriser ou non l\'affichage du formulaire par défaut, par exemple lors de l\'utilisation d\'un plugin différent.</li>
                        <li>- Définition de l\'emplacement ou afficher le lien d\'accès au formualire de contact.</li>
                    </ul>
                </li> 
                <li>Toutefois, pour utiliser cette fonctionnalités, vous devez avoir un compte/auteur attitré.</li>
            </ul>
        </div>';
?>