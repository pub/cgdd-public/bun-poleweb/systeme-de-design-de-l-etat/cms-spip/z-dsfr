<?php
    echo '
    <ul class="masquer_liens_obligation_legale_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Masquer les liens obligation légale"</h3></li>
        <li>Dans le footer il existe une liste de liens liés aux obligations légales.</li>
        <li>Cette liste doit être définie en fonction du site; toutefois les liens & contenus suivants sont obligatoires : <br>
        - “accessibilité : non/partiellement/totalement conforme”, <br>
        - mentions légales, <br>
        - données personnelles<br>
        - gestion des cookies. - obligatoire.</li>
        <li>Notez bien que concernant la gestion des cookies, la création de ce lien n\'est pas nécessaire si vous avez choisi "oui" dans le la commande "Masquer cookies". </li>
        <li><a href="'.$_GET['chemin_plugin'].'z-dsfr/design_system_admin/prive/themes/spip/images/cookies.jpg" target="_blank" title="afficher l\'image - nouvelle fenêtre"><img src="'.$_GET['chemin_plugin'].'z-dsfr/design_system_admin/prive/themes/spip/images/cookies.jpg" width="250" /></a><br>
        <a href="'.$_GET['chemin_plugin'].'z-dsfr/design_system_admin/prive/themes/spip/images/obligation_legale_liens.jpg" target="_blank" title="afficher l\'image - nouvelle fenêtre"><img src="'.$_GET['chemin_plugin'].'z-dsfr/design_system_admin/prive/themes/spip/images/obligation_legale_liens.jpg" width="250" /></a>
        </li>
    </ul>';
    
?>