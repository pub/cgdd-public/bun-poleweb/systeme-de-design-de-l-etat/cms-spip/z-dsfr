<?php
    echo '<div class="aide_onglet_navigateur aide_masquer_langue masquer_langue_secondaire">
            <ul>
                <li><h3>Aide sur la fonctionnalité "Masquer langue?"</h3></li>
                <li>Cette fonctionnalité permet de gérer l\'internationalisation du site.</li>                
                <li>Le fonctionnement de cette fonctionnalité se base sur l\'utilisation de l\'élément traduction de SPIP.</li> 
            </ul>
        </div>';
?>