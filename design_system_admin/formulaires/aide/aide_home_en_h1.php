<?php
    echo '
    <ul class="home_en_h1_secondaire secondaire_help">
        <li><h3>Aide sur l’utilisation de la fonctionnalité "Mettre le titre de la page d’accueil en H1"</h3></li>
        <li>Cette fonctionnalité est une fonctionnalité inédite de ce thème, dans la mesure où elle n’appartient en aucune façon au Design system de l’état.</li>
        <li>Il peut arriver que vous ne souhaitiez pas afficher un titre sur le contenu de votre page d’accueil. Hors il est obligatoire d’avoir un H1 sur toutes les pages y compris la page d’accueil.</li>
        <li>Si vous êtes dans ce cas, activez cette fonctionnalité et le titre du site passera automatiquement en H1 (bien entendu, ceci n’est réservé qu’à la page d’accueil).</li>
    </ul>';
    
?>