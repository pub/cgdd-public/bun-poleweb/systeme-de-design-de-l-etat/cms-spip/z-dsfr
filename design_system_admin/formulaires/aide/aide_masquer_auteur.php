<?php
    echo '<div class="aide_onglet_navigateur aide_masquer_auteur masquer_auteur_secondaire ">
            <ul>
                <li><h3>Aide sur le champ "Masquer auteur"</h3></li>
                <li>Par défaut, SPIP fournit la possibilité d\'afficher le profil de l\'auteur.</li>                
                <li>Nous avons complexifié un peu la fonctionnalité; désormais, vous avez les choix suivants:
                    <ul>
                        <li>- Ne pas mettre en place la fonctionnalité, dans ce cas, quand un utilisateur essaiera d\'accéder à cette page, il sera automatiquement redirigé vers la page d\'accueil.</li>
                        <li>- Vous pouvez aussi autoriser ou non l\'affichage d\'un formulaire de contact à destination de l\'auteur.</li>
                    </ul>
                </li> 
            </ul>
        </div>';
?>