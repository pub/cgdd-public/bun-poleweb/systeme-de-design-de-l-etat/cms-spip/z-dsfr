<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/**
 * Gestion du formulaire de d'édition de dsfrconfiguration
 *
 * @plugin     Design system Admin
 * @copyright  2023
 * @author     Mikaël Folio
 * @licence    GNU/GPL
 * @package    SPIP\Design_system_admin\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

// Inclure les fichiers nécessaires ici
include_spip('inc/cvtupload');
include_spip('action/ajouter_documents');  // Inclure la bibliothèque nécessaire.

/*if (test_espace_prive()) {
    // Vérifier si le formulaire a été soumis


}*/
if (session_status() == PHP_SESSION_ACTIVE) {
    // Session is started
    // Perform your actions here
    // Access session variables using $_SESSION
} else {
    // Session is not started
    // Start the session
    session_start();
}

/**
 * Déclaration des saisies de dsfrconfiguration
 *
 * @param int|string $id_dsfrconfiguration
 *     Identifiant du dsfrconfiguration. 'new' pour un nouveau dsfrconfiguration.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un dsfrconfiguration source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du dsfrconfiguration, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array[]
 *     Saisies du formulaire
 */
function formulaires_configurer_design_system_admin_saisies_dist($id_dsfrconfiguration = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$saisies = [
	];
	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_dsfrconfiguration
 *     Identifiant du dsfrconfiguration. 'new' pour un nouveau dsfrconfiguration.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un dsfrconfiguration source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du dsfrconfiguration, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_configurer_design_system_admin_identifier_dist($id_dsfrconfiguration = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	return json_encode([intval($id_dsfrconfiguration)]);
}

/**
 * Chargement du formulaire d'édition de dsfrconfiguration
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_dsfrconfiguration
 *     Identifiant du dsfrconfiguration. 'new' pour un nouveau dsfrconfiguration.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un dsfrconfiguration source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du dsfrconfiguration, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_configurer_design_system_admin_charger_dist($id_dsfrconfiguration = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$valeurs = formulaires_editer_objet_charger('dsfrconfiguration', $id_dsfrconfiguration, '', $lier_trad, $retour, $config_fonc, $row, $hidden);

    $args = func_get_args();
	$valeurs['saisies'] = formulaires_configurer_design_system_admin_saisies_dist(...$args);
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de dsfrconfiguration
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_dsfrconfiguration
 *     Identifiant du dsfrconfiguration. 'new' pour un nouveau dsfrconfiguration.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un dsfrconfiguration source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du dsfrconfiguration, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_configurer_design_system_admin_verifier_dist($id_dsfrconfiguration = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {

	$erreurs = formulaires_editer_objet_verifier('dsfrconfiguration', $id_dsfrconfiguration);


	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de dsfrconfiguration
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_dsfrconfiguration
 *     Identifiant du dsfrconfiguration. 'new' pour un nouveau dsfrconfiguration.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un dsfrconfiguration source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du dsfrconfiguration, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 */
function formulaires_configurer_design_system_admin_traiter_dist($id_dsfrconfiguration = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
    //var_dump($_POST);
    //var_dump($_FILES);
    
    //$retours = formulaires_editer_objet_traiter('dsfrconfiguration', $id_dsfrconfiguration, '', $lier_trad, $retour, $config_fonc, $row, $hidden);
    $retours = array();
    // Liste des champs du formulaire à sauvegarder
    $fields = [
        'nom_de_domaine_en_production',
        'title_onglet_navigateur',
        'meta_description',
        'address_email_responsable',
        'form_contact',
        'texte_marianne',
        'liens_externe_que_faire',
        '_masquer_le_titre',
        'titre_du_site',
        '_masquer_le_slogan',
        'slogan_du_site',
        '_drapeau_en_berne',
        '_masquer_haut_de_page',
        '_masquer_auteur',
        'contact_auteur',
        '_masquer_contact',
        'titre_lien_contact',
        'id_auteur_contact',
        'emplacement_contact',
        '_home_en_h1',
        '_masquer_recherche',
        '_masquer_message_alerte',
        'message_alerte',
        '_masquer_statistiques',
        'script_statistiques',
        '_masquer_partage_reseaux_sociaux',
        'titre_partage_reseaux_sociaux',
        'url_facebook',
        'url_twitter',
        'url_youtube',
        'url_instagram',
        'url_linkedin',
        'url_rss',
        '_masquer_la_newsletter',
        'titre_form_newsletter',
        'slogan_form_newsletter',
        'description_newsletter',
        'type_formulaire_newsletter',
        'url_formulaire_newsletter',
        'titre_bouton_sabonner',
        'nom_champ_input_form',
        '_masquer_parametres_affichage',
        'parametre_affichage_defaut',
        'pad_dans_footer',
        '_masquer_cookies',
        'liste_des_cookies',
        'modifier_texte_cookies',
        'titre_texte_cookies',
        'texte_cookies',
        '_masquer_liste_acces_rapide',
        '_masquer_liste_de_lien',
        'lien_dacces_rapide1',
        'lien_dacces_rapide2',
        'lien_dacces_rapide3',
        'nombre_de_liste_de_liens',
        'type_de_liste_de_liens',
        'liste_de_liens_1',
        'liste_de_liens_2',
        'liste_de_liens_3',
        'liste_de_liens_4',
        'liste_de_liens_5',
        'liste_de_liens_6',  
        '_masquer_menu',      
        'type_de_menu',
        'nombre_de_menu',
        'menu_1',
        'menu_2',
        'menu_3',
        'menu_4',
        'menu_5',
        'menu_6',
        'menu_7',
        'menu_8',
        'menu_9',
        '_masquer_partenaires',
        'partenaires_principaux_1',
        'partenaires_principaux_2',
        'partenaires_secondaires_1',
        'partenaires_secondaires_2',
        'partenaires_secondaires_3',
        'partenaires_secondaires_4',
        'nombre_de_partenaires_principaux',
        'nombre_de_partenaires_secondaires',
        'nombre_de_liens_ecosysteme',
        'lien_eco_1',
        'lien_eco_2',
        'lien_eco_3',
        'lien_eco_4',
        'nombre_de_liens_obligations_legales',
        'lien_legal_1',
        'lien_legal_2',
        'lien_legal_3',
        'lien_legal_4',
        'lien_legal_5',
        'lien_legal_6',
        'lien_legal_7',
        'lien_legal_8',
        'nombre_de_liens_utiles',
        'afficher_bouton_cookies',
        'dsfr_licence',
        'description_footer',
        'titre_du_bloc_partenaires',
        '_masquer_logo_secondaire',
        'second_logo_upload',
        'alt_second_logo_upload',
        'type_insertion_second_logo',
        'elements_recherche',
        '_masquer_langue',
        'nombre_de_langue',
        'langue_par_defaut',
        'langue_1',
        'value_langue_1',
        'langue_2',
        'value_langue_2',
        'langue_3',
        'value_langue_3',
        'langue_4',
        'value_langue_4',
        'langue_5',
        'value_langue_5'
        
        
    ];
    // Boucle sur chaque champ
    foreach ($fields as $field) {
        // Vérifie si le champ POST existe pour ce champ
        if (isset($_POST[$field])) {
        
            // Récupération de la valeur du champ
            $value = _request($field);
            
            // Recherche si l'option existe déjà dans la table
            $query = sql_select("id_dsfrconfiguration", "spip_dsfrconfigurations", "option_name = ".sql_quote($field));
            $row = sql_fetch($query);
            
            if ($row) { // Si l'option existe déjà, on la met à jour
                sql_updateq("spip_dsfrconfigurations", array("option_value" => $value), "option_name = ".sql_quote($field));
            } else { // Sinon, on crée une nouvelle option
                sql_insertq("spip_dsfrconfigurations", array("option_name" => $field, "option_value" => $value));
            }
        }
    }
    
    // Parcourir tous les fichiers téléchargés.
    include_spip('action/ajouter_documents');  // Inclure la bibliothèque nécessaire.

    // Parcourir tous les fichiers téléchargés.
    // Vérifiez s'il y a des fichiers téléchargés
    if (!empty($_FILES)) {
        // Parcourir tous les fichiers téléchargés
        foreach ($_FILES as $key => $file) {
            $tmp_name = $file['tmp_name'];
            $name = $file['name'];

            // Vérifiez que la clé 'error' existe avant d'y accéder
            if (!isset($file['error'])) {
                // Gérer le cas où 'error' n'est pas défini
                // Par exemple, vous pouvez définir $error à une valeur par défaut
                $error = UPLOAD_ERR_NO_FILE; // Supposons qu'il n'y a pas de fichier s'il n'y a pas d'erreur définie
            } else {
                $error = $file['error'];
            }
        
            // Vérifier le type d'erreur.
            switch ($error) {
                case UPLOAD_ERR_OK:
                    // Aucune erreur, le téléchargement a réussi.
                    if (is_uploaded_file($tmp_name)) {
                        // Déplacer le fichier dans le répertoire IMG/.
                        $dest = _DIR_IMG . $name;
                        $ok = move_uploaded_file($tmp_name, $dest);
        
                        if ($ok) {
                            // Le fichier a été déplacé avec succès.
                            // Vous pouvez maintenant faire ce que vous voulez avec le fichier.
                            $_SESSION[$key] = $name;
                            // Vérifier si un enregistrement avec le même nom de fichier existe déjà
                            $existing_file = sql_fetsel('option_value', 'spip_dsfrconfigurations', 'option_name = ' . sql_quote($key));

                            if ($existing_file) {
                                // Un enregistrement avec le même nom de fichier existe déjà
                                // Effectuer l'action appropriée, par exemple une mise à jour de l'enregistrement existant
                                sql_updateq('spip_dsfrconfigurations', array('option_value' => $name), 'option_name = ' . sql_quote($key));
                            } else {
                                // Aucun enregistrement avec le même nom de fichier, procéder à l'insertion
                                sql_insertq('spip_dsfrconfigurations', array('option_name' => $key, 'option_value' => $name));
                            }
                        } else {
                            // Il y a eu une erreur lors du déplacement du fichier.
                            // Gérer cette erreur ici.
                            $retours['message_erreur'] = 'Erreur lors du déplacement du fichier !!!';
                            return $retours;
                        }
                    }
                    break;
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    // Le fichier téléchargé dépasse la directive upload_max_filesize dans php.ini, ou la directive MAX_FILE_SIZE spécifiée dans le formulaire HTML.
                    // Gérer cette erreur ici.
                    $retours['message_erreur'] = 'Le fichier téléchargé dépasse la taille limite imposée !!!';
                    return $retours;
                    break;
                case UPLOAD_ERR_PARTIAL:
                    // Le fichier n'a été que partiellement téléchargé.
                    // Gérer cette erreur ici.
                    $retours['message_erreur'] = 'Le téléchargement du fichier s\'est terminé anormalement !!!';
                    return $retours;
                    break;
                case UPLOAD_ERR_NO_FILE:
                    // Aucun fichier n'a été téléchargé.
                    // Gérer cette erreur ici.
                    $retours['message_erreur'] = 'Aucun fichier n\'a été téléchargé !!!';
                    return $retours;
                    break;
                case UPLOAD_ERR_NO_TMP_DIR:
                    // Absence de dossier temporaire.
                    // Gérer cette erreur ici.
                    $retours['message_erreur'] = 'Le dossier temporaire défini est absent !!!';
                    return $retours;
                    break;
                case UPLOAD_ERR_CANT_WRITE:
                    // Échec de l'écriture du fichier sur le disque.
                    // Gérer cette erreur ici.
                    $retours['message_erreur'] = 'Échec de l\'écriture du fichier sur le disque !!!';
                    return $retours;
                    break;
                case UPLOAD_ERR_EXTENSION:
                    // Une extension PHP a arrêté le téléchargement du fichier.
                    // Gérer cette erreur ici.
                    $retours['message_erreur'] = 'Une extension PHP a empêché le téléchargement du fichier !!!';
                    return $retours;
                    break;
                default:
                    // Une erreur inconnue s'est produite.
                    // Gérer cette erreur ici.
                    $retours['message_erreur'] = 'Une erreur inconnue s\'est produite !!!';
                    return $retours;
                    break;
            }
        }
    }
    // Ajout d'un message de succès personnalisé dans le tableau de retours
    $retours['message_ok'] = 'Les modifications ont bien été prises en compte et sauvegardées.';
    return $retours;
}
