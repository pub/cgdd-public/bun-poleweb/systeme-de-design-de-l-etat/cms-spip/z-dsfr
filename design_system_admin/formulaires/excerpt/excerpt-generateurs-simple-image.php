<?php
/**
 * Template part for displaying posts of the format "Actualités"
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
?>

<?php 
    // Générateur identifiant unique
    function generate_unique_id() {
        //$prefix = 'my_id_';
        $suffix = uniqid();
        $unique_id = $suffix;
        return $unique_id;
        }
        
$idgen = generate_unique_id();
?>
<li class="simple_icone_flag_remove">
    <form method="post" id="element_generator">
        <input type="hidden" name="type_generateur" id="type_generateur" value="<?php echo $_GET['typeGenerateur']; ?>" />
        <input type="hidden" name="element_parent" id="element_parent" value="<?php echo $_GET['componentFille']; ?>" />
        <ul class="form_configuration resultat_menu_simple les-resultats dontshow">
        <li></li>
        <li>
            <fieldset>
            <legend>Résultat obtenu</legend>
            <nav class="fr-nav" id="navigation-<?php echo $idgen; ?>" role="navigation" aria-label="Menu principal" data-fr-js-navigation="true">
                <ul class="fr-nav__list">
                <li class="fr-nav__item" data-fr-js-navigation-item="true">
                    <a href="xxxxxx" class="button-home ">xxxxxx</a>
                </li>         
                </ul>
            </nav>
            </fieldset>
        </li>
        </ul>
        <ul class="form_commandes">      
        <li>
            <label for="menu-simple-titre"><span class="redbold">*</span> Nom de l'élément:</label><br>
            <input type="text" name="menu-simple-titre" id="menu-simple-titre" class="width100 obligatoire" placeholder="Ex: Accueil" autocomplete="off" required="required"/>
        </li>
        <li>
            <label for="menu-simple-url"><span class="redbold">*</span> URL de l'élément:</label><br>
            <input type="text" name="menu-simple-url" id="menu-simple-url" class="width100" placeholder="Ex: https://... ou /xxxxxx" autocomplete="off"   required="required"/>
        </li>
        <li class="menu_simple_image">
                <label for="menu-simple-icon">Url du logo ou de l'image</label><br>
                <input type="text" name="menu-simple-icon" id="menu-simple-icon" class="width100 obligatoire" placeholder="https://..." autocomplete="off"/>
            </li>
        </ul>
    </form>
</li>



