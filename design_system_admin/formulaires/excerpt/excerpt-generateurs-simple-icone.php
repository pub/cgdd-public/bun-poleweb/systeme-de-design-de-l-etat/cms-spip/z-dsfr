<?php
/**
 * Template part for displaying posts of the format "Actualités"
 *
 * @package WordPress
 * @subpackage Wordpress_DSFR_Project
 * @since Wordpress_DSFR_Project 1.0
 */
?>

<?php 
	// Générateur identifiant unique
	function generate_unique_id() {
        //$prefix = 'my_id_';
        $suffix = uniqid();
        $unique_id = $suffix;
        return $unique_id;
        }
        
  $idgen = generate_unique_id();
?>
  <li class="simple_icone_flag_remove">
    <input type="hidden" name="type_generateur" id="type_generateur" value="<?php echo $_GET['typeGenerateur']; ?>" />
    <input type="hidden" name="element_parent" id="element_parent" value="<?php echo $_GET['componentFille']; ?>" />
    <ul class="form_commandes">      
      <li>
        <label for="menu-simple-titre"> Titre du lien (obligatoire):</label>
        <input type="text" name="menu-simple-titre" id="menu-simple-titre" class="text width100" placeholder="Ex: Accueil" autocomplete="off" required="required"/>
      </li>
      <li>
        <label for="menu-simple-url">URL du lien (obligatoire):</label>
        <input type="text" name="menu-simple-url" id="menu-simple-url" class="text width100" placeholder="Ex: https://... ou /xxxxxx" autocomplete="off" required="required"/>
      </li>
      <li>
        <label for="menu-simple-icon">Icône DSFR:</label>
        <input type="text" name="menu-simple-icon" id="menu-simple-icon" class="text width100" placeholder="Icônes du DSFR, exemple : fr-icon-question-fill" autocomplete="off"/>
        <label>Consulter <a href="https://www.systeme-de-design.gouv.fr/elements-d-interface/fondamentaux-techniques/icone/" target="_blank" rel="external" titre="aller sur la page d'icônes du DSFR - nouvelle fenêtre">les icônes</a> disponibles.
      </li>
    </ul>
  </li>



