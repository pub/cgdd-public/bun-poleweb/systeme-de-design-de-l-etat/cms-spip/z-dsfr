<?php
echo '
    <li class="_masquer_liens_obligation_legale">
        <ul class="gestion_masquer_liens_obligation_legale" style="display:grid;">
            <li class="_masquer_liens_obligation_legale">
                <div class="editer editer_nombre_de_liens_obligations_legales saisie_selection editer_odd">
                    <label class="editer-label" for="champ_nombre_de_liens_obligations_legales">Nombre de liens obligations légales</label>
                    <select name="nombre_de_liens_obligations_legales" id="champ_nombre_de_liens_obligations_legales" data-elementname="lien_legal" data-parent="masquer_liens_obligation_legale" data-select="numeric" data-select-0="1" required="required">                        
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                    </select>
                </div>
            </li>            
        </ul>
        <ul class="_masquer_liens_obligation_legale_1">
           <li class="_masquer_liens_obligation_legale">
                <ul >
                    <li class="_masquer_liste_de_lien liste_externe">
                        <div class="editer editer_lien_legal_1 obligatoire saisie_input editer_even">
                            <label class="editer-label" for="champ_lien_legal_1">Lien légal 1<span class="obligatoire"> (obligatoire)</span></label>
                            <input type="text" name="lien_legal_1" class="text" id="champ_lien_legal_1" required="required" data-parent="masquer_menu" placeholder="Cliquer sur le bouton pour configurer cet élément">
                            <button class="liste_de_lien configurer_element" title="Cliquer sur le bouton pour configurer cet élément"><img src="'.$_GET['chemin_plugin'].'z-dsfr/design_system_admin/prive/themes/spip/images/settings.svg" width="12" height="12"/>
                        </div>
                    </li>                    
                </ul>
            </li>
        </ul>
    </li>
';  
?>
