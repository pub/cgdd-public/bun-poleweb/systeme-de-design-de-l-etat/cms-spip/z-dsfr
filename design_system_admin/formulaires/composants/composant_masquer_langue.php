<?php
echo '
    <li class="_masquer_langue">
        <div class="editer editer_nombre_de_langue saisie_selection editer_odd">
            <label class="editer-label" for="champ_nombre_de_langue">Nombre de langues</label><br>
            <select name="nombre_de_langue" id="champ_nombre_de_langue" data-elementname="langue" data-parent="masquer_langue" data-select="numeric" data-select-0="1" required="required">
                <option value="1" selected="selected">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
        </div>
    </li>
    <li class="_masquer_langue">
        <div class="editer editer_langue_defaut obligatoire saisie_input editer_even">
            <label class="editer-label" for="champ_langue_defaut">Langue par défaut<span class="obligatoire"> (obligatoire)</span></label>
            <input type="text" name="langue_par_defaut" class="text" id="champ_langue_defaut" value="" required="required" data-parent="masquer_langue" placeholder="Langue par défaut (fr, en, ...)">																			
        </div>
    </li>
    <li class="_masquer_langue">
        <ul class="gestion_langue">
            <li class="_masquer_langue masquer_langue_1">
                <div class="editer editer_langue_1 obligatoire saisie_input editer_even">
                    <label class="editer-label" for="champ_langue_1">Langue 1<span class="obligatoire"> (obligatoire)</span></label>
                    <input type="text" name="langue_1" class="text" id="champ_langue_1" required="required" data-parent="masquer_langue" placeholder="Titre langue 1">                    
                    <input type="text" name="value_langue_1" class="text" id="value_langue_1" required="required" data-parent="masquer_langue" placeholder="Valeur langue 1 (fr, en, ...)">
                </div>
            </li>     
        </ul>
    </li>';    
?>