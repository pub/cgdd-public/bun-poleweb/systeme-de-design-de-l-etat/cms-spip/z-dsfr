<li class="_masquer_contact">
    <div class="editer editer_titre_lien_contact obligatoire editer_odd">
        <label class="editer-label" for="titre_lien_contact">Titre du lien contact<span class="obligatoire"> (obligatoire)</span></label>
        <input type="text" name="titre_lien_contact" class="text" id="titre_lien_contact" value="" required="required" data-parent="masquer_contact" placeholder="Champ obligatoire. Titre du lien contact à afficher dans les menus">							
    </div>
</li>
<li class="_masquer_contact">
    <div class="editer editer_id_auteur_contact obligatoire  editer_odd">
        <label class="editer-label" for="id_auteur_contact">ID de l'auteur<span class="obligatoire"> (obligatoire)</span></label>
        <input type="text" name="id_auteur_contact" class="text" id="id_auteur_contact" value="" required="required" data-parent="masquer_contact" placeholder="Champ obligatoire. ID de l'auteur à qui envoyer ce formulaire par email">							
    </div>
</li>
<li class="_masquer_contact">
    <div class="editer editer_emplacement_contact obligatoire editer_odd">
        <label class="editer-label" for="emplacement_contact">Ou placer le lien contact?<span class="obligatoire"> (obligatoire)</span></label>
        <select name="emplacement_contact" id="emplacement_contact" required="required">
            <option value="1">Liens rapides(Header) + Menu Header + Obligations légales(Footer)</option>
            <option value="2">Liens rapides(Header) + Menu Header</option>
            <option value="3">Menu Header + Obligations légales(Footer)</option>
            <option value="4">Liens rapides(Header) + Obligations légales(Footer)</option>
            <option value="5">Liens rapides(Header)</option>
            <option value="6">Menu Header</option>
            <option value="7">Obligations légales(Footer)</option>
        </select>
    </div>
</li>
<li class="_masquer_contact">
    <hr class="hr width100">
    <span class="bold">Notes:</span>
</li>
<li class="_masquer_contact">
    La page de contact par défaut renvoie vers contact.html.<br><br>
    Pour utiliser le formulaire par défaut, vous devez avoir un compte/auteur et indiquer l'id de ce compte/auteur pour l'envoi des emails.<br><br>
    Si vous utilisez votre propre fichier, masquer cette fonctionnalité et placer votre fichier dans le dossier plugins-dist/z-dsfr/design_system_child/.<br><br>
    Dans le cas de l'utilisation d'un plugin (exempel Formidable,...), désactiver uniquement cette fonctionnalité et suivre les instructions du plugin utilisé.

</li>
