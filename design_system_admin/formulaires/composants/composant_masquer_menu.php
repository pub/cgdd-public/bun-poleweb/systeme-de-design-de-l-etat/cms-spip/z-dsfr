<?php
echo '
    <li class="_masquer_menu">
        <div class="editer editer_nombre_de_menu saisie_selection editer_odd">
            <label class="editer-label" for="nombre_de_menu">Nombre de menus</label><br>
            <select name="nombre_de_menu" id="nombre_de_menu" data-select="numeric" data-parent="masquer_menu" data-elementname="menu">
                <option value="1" selected="selected">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
            </select>
        </div>
    </li>
    <li class="_masquer_menu" style="order:1;">
        <ul class="l8020">
            <li>
                <div class="editer editer_menu_1 obligatoire saisie_input editer_even">
                    <label class="editer-label" for="champ_menu_1">Menu 1<span class="obligatoire"> (obligatoire)</span></label>
                    <input type="text" name="menu_1" class="text" id="champ_menu_1" required="required" data-parent="masquer_menu" placeholder="Cliquer sur le bouton pour configurer cet élément">
                    <button class="liste_de_lien configurer_element" title="Cliquer sur le bouton pour configurer cet élément"><img src="'.$_GET['chemin_plugin'].'z-dsfr/design_system_admin/prive/themes/spip/images/settings.svg" width="12" height="12">
                    </button>
                </div>
            </li>     
            <li></li>
        </ul>
    </li>';    
?>