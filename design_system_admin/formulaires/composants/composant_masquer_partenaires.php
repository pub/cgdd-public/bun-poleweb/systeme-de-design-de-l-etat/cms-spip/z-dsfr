<?php
echo '
    <li class="_masquer_partenaires">
        <div class="editer editer_titre_du_bloc_partenaires saisie_input editer_odd">
            <label class="editer-label" for="champ_titre_du_bloc_partenaires">Titre bloc « Partenaires »</label>
            <input type="text" name="titre_du_bloc_partenaires" class="text" id="champ_titre_du_bloc_partenaires" placeholder="Saisir le titre du bloc partenaires">
        </div>									
    </li>	
    <li class="_masquer_partenaires">
        <h3>Les partenaires principaux</h3>
    </li>
    <li class="_masquer_partenaires">
        <div class="editer editer_nombre_de_partenaires_principaux saisie_selection editer_odd">
            <label class="editer-label" for="champ_nombre_de_partenaires_principaux">Nombre de partenaires principaux</label>
            <select name="nombre_de_partenaires_principaux" id="champ_nombre_de_partenaires_principaux" data-elementname="partenaires_principaux" data-parent="partenaires_principaux" data-select="numeric" data-select-0="0">
                <option value="0">0</option>
                <option value="1" selected="selected">1</option>
                <option value="2">2</option>
            </select>
        </div>
    </li>
    <li class="_masquer_partenaires">
        <ul class="gestion_partenaires_principaux l8020">
            <li class="partenaires_principaux_1">
                <div class="editer editer_partenaires_principaux_1 obligatoire saisie_input editer_even">
                    <label class="editer-label" for="champ_partenaires_principaux_1">Partenaire principal 1<span class="obligatoire"> (obligatoire)</span></label>
                    <input type="text altkey" name="partenaires_principaux_1" class="text" id="champ_partenaires_principaux_1" placeholder="Cliquer sur le bouton pour configurer cet élément" required="required" data-parent="masquer_partenaires">
                    <button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_partenaires" data-fille="partenaires_principaux_1" data-type-generateur="simple-image"><img src="'.$_GET['chemin_plugin'].'z-dsfr/design_system_admin/prive/themes/spip/images/settings.svg" width="28" height="28"></button>
                </div>
            </li>
            <li class="partenaires_principaux_1"></li>
        </ul>
    </li>
    <li class="_masquer_partenaires">
        <h3>Les partenaires secondaires</h3>
    </li>
    <li class="_masquer_partenaires">
        <div class="editer editer_nombre_de_partenaires_secondaires saisie_selection editer_odd">
            <label class="editer-label" for="champ_nombre_de_partenaires_secondaires">Nombre de partenaires secondaires</label>
            <select name="nombre_de_partenaires_secondaires" id="champ_nombre_de_partenaires_secondaires" data-elementname="partenaires_secondaires" data-parent="partenaires_secondaires" data-select="numeric" data-select-0="0">
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2" selected="selected">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
            </select>
        </div>
    </li>
    <li class="_masquer_partenaires">
        <ul class="gestion_partenaires_secondaires l8020">
            <li class="partenaires_secondaires_1">
                <div class="editer editer_partenaires_secondaires_1 obligatoire saisie_input editer_even">
                    <label class="editer-label" for="champ_partenaires_secondaires_1">Partenaire secondaire 1<span class="obligatoire"> (obligatoire)</span></label>
                    <input type="text altkey" name="partenaires_secondaires_1" class="text" id="champ_partenaires_secondaires_1" value="" required="required" data-parent="masquer_partenaires" placeholder="Cliquer sur le bouton pour configurer cet élément" >
                    <button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_partenaires" data-fille="partenaires_secondaires_1" data-type-generateur="simple-image"><img src="'.$_GET['chemin_plugin'].'z-dsfr/design_system_admin/prive/themes/spip/images/settings.svg" width="28" height="28"></button>
                </div>
            </li>
            <li class="partenaires_secondaires_1"></li>
            <li class="partenaires_secondaires_2">
                <div class="editer editer_partenaires_secondaires_2 obligatoire saisie_input editer_odd">
                    <label class="editer-label" for="champ_partenaires_secondaires_2">Partenaire secondaire 2<span class="obligatoire"> (obligatoire)</span></label>
                    <input type="text altkey" name="partenaires_secondaires_2" class="text" id="champ_partenaires_secondaires_2" value="" required="required" data-parent="masquer_partenaires" placeholder="Cliquer sur le bouton pour configurer cet élément" >
                    <button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_partenaires" data-fille="partenaires_secondaires_2" data-type-generateur="simple-image"><img src="'.$_GET['chemin_plugin'].'z-dsfr/design_system_admin/prive/themes/spip/images/settings.svg" width="28" height="28"></button>
                </div>
            </li>
            <li class="partenaires_secondaires_2"></li>
        </ul>
    </li>';  
?>