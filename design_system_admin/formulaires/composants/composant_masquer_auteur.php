<li class="_masquer_auteur">
	<div class="editer editer_auteur obligatoire saisie_textarea editer_odd">
		<label class="editer-label" for="contact_auteur">Contacter l'auteur<span class="obligatoire"> (obligatoire)</span></label>
        <select name="contact_auteur" id="champ_contact_auteur" required="required">
            <option value="non">Non</option>
            <option value="oui">oui</option>
        </select>
    </div>
</li>