<?php
    echo '
    <li class="_masquer_logo_secondaire">
        <div class="editer editer_liens_externe_que_faire saisie_selection editer_odd">
            <label class="editer-label" for="type_insertion_second_logo">Type image (fichier ou code svg)? </label>
            <select name="type_insertion_second_logo" id="type_insertion_second_logo">
                <option value="fichier" selected="selected">url de votre image</option>
                <option value="code">code<option>
            </select>
        </div>
    </li>
    <li class="_masquer_logo_secondaire suppression_image">
        <div class="editer editer_second_logo_upload obligatoire">
            <label for="second_logo_upload">Lien de votre image (obligatoire)</label>
            <textarea name="second_logo_upload" id="second_logo_upload" rows="6" cols="33" placeholder="Saisir le lien du fichier ou le code svg" required="required" data-parent="masquer_logo_secondaire"></textarea>							
        </div>						
    </li>
    <li class="_masquer_logo_secondaire suppression_image">
        <div class="editer editer_alt_second_logo_upload obligatoire">
            <label for="second_logo_upload">Alternative de votre image(obligatoire)</label>
            <input type="text" class="text width100" name="alt_second_logo_upload" id="alt_second_logo_upload" required="required"  data-parent="masquer_logo_secondaire" value="" placeholder="Saisir une alternative texxtuelle de votre image"/>
        </div>						
    </li>';
?>
