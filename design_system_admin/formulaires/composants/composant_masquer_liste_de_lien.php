<?php
echo '
    <li class="_masquer_liste_de_lien">
        <ul class="gestion_liste_de_liens" style="display:grid;">
            <li class="_masquer_liste_de_lien nbr_liste">
                <div class="editer editer_nombre_de_liste_de_liens saisie_selection editer_odd">
                    <label class="editer-label" for="nombre_de_liste_de_liens">Nombre de liste de liens</label><br>
                    <select name="nombre_de_liste_de_liens" id="nombre_de_liste_de_liens" data-elementname="liste_de_liens" data-select="numeric" data-parent="masquer_liste_de_lien" data-select-0="1">
                    <option value="1" selected="selected">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>
                </div>
            </li>
            <li class="_masquer_liste_de_lien liste_externe  masquer_liste_de_lien_1" style="order:1;">
                <ul class="l8020">
                    <li class="_masquer_liste_de_lien liste_externe">
                        <div class="editer editer_liste_de_liens_1 obligatoire saisie_input editer_odd">
                            <label class="editer-label" for="champ_liste_de_liens_1">Liste de liens 1<span class="obligatoire"> (obligatoire)</span></label>
                            <input type="text" name="liste_de_liens_1" class="text altkey" id="champ_liste_de_liens_1" value="" required="required" data-parent="masquer_liste_de_lien" placeholder="Cliquer sur le bouton pour configurer cet élément">
                            <button class="configurer_element" type="button" title="Configurer cet élément" data-parent="masquer_liste_de_lien_primaire" data-fille="liste_de_liens_1" data-type-generateur="liste_de_liens"><img src="'.$_GET['chemin_plugin'].'z-dsfr/design_system_admin/prive/themes/spip/images/settings.svg" width="12" height="12"></button>
                        </div>
                    </li>                    
                </ul>
            </li>
        </ul>
    </li>
';  
?>
