<?php
    $menuSimpleUrl = isset($_GET['menuSimpleUrl']) ? $_GET['menuSimpleUrl'] : '';
    $menuSimpleIcon = isset($_GET['menuSimpleIcon']) ? $_GET['menuSimpleIcon'] : '';
    $menuSimpleTitre = isset($_GET['menuSimpleTitre']) ? $_GET['menuSimpleTitre'] : '';

    echo '
        <nav class="fr-nav" id="resultats" role="navigation" aria-label="Menu principal" data-fr-js-navigation="true">
            <ul class="fr-nav__list">
                <li class="fr-nav__item" data-fr-js-navigation-item="true">
                    <a href="' .  $menuSimpleUrl. '" class="button-home">' .((strlen($menuSimpleIcon) > 0) ? '<img src="'.$menuSimpleIcon.'" width="200" alt="'.$menuSimpleTitre.'" />' : $menuSimpleTitre). '</a>
                </li>
            </ul>
        </nav>
    ';
?>


