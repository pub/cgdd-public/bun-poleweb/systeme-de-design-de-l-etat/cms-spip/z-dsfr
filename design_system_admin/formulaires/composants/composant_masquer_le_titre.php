<?php
echo '
    <li class="_masquer_le_titre champ_titre_du_site">
        <div class="editer editer_titre_du_site obligatoire saisie_input editer_odd">
            <label class="editer-label" for="champ_titre_du_site">Titre du site<span class="obligatoire"> (obligatoire)</span></label>
            <input type="text" name="titre_du_site" class="text" id="champ_titre_du_site" value="" required="required" data-parent="masquer_le_titre" placeholder="Champ obligatoire. Saisir le titre du site">
        </div>
    </li>';  
?>