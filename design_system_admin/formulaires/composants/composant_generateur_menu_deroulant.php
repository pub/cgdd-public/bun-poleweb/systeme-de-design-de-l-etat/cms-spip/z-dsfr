
<?php
    function generateUuidV4() {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
    echo'
    <ul class="affichage_generateur">
        <li id="simple_menu_nav">
            <style>
                input.text, textarea, select {
                position: relative;
                z-index: 2;
                padding: var(--spip-form-input-padding-y) var(--spip-form-input-padding-x);
                margin: 0;
                width: 100%;
                border: 1px solid var(--spip-form-input-border-color);
                border-radius: var(--spip-form-input-border-radius);
                background-color: var(--spip-color-white);
                transition: box-shadow 0.1s;
            }
            .affichage_generateur{
                margin-top:1.5rem;
            }
            .dontshow{
                display: none !important;
            }
            input.button-primary {
                background: #2271b1;
                border-color: #2271b1;
                color: #fff;
                text-decoration: none;
                text-shadow: none;
                display: inline-block;
                font-size: 13px;
                line-height: 2.15384615;
                min-height: 30px;
                margin: 0;
                padding: 0 10px;
                cursor: pointer;
                border-width: 1px;
                border-style: solid;
                -webkit-appearance: none;
                border-radius: 3px;
                white-space: nowrap;
                box-sizing: border-box;
                width:80% !important;
            }

            
            .button-home {
                padding: 1rem;
                background-color: white;
                color: black;
                display: inline-block;
                text-decoration: none;
            }
            .button-home:hover {
                background-color: #f6f6f6;
            }
            a:active, a:hover {
                color: #135e96;
            }
            </style>
            <input type="hidden" name="type_generateur" id="type_generateur" value="'.$_GET['type_generateur'].'" />
            <input type="hidden" name="element_parent" id="element_parent" value="'.$_GET['element_parent'].'" />
            <ul>
                <li class="simple_icone_flag_remove show_menu_type_select affichage_generateur">
                    <label for="list-menu-type">Sélection du type de menu</label>
                    <select name="list-menu-type" id="list-menu-type" data-emplacement="elementFormUl" data-type_generateur="menu_deroulant" data-element_parent="menu_1">
                        <option value="menu">Lien simple</option>
                        <option value="menu_deroulant" selected="selected">Menu déroulant</option>
                        <option value="megamenu">Mégamenu</option>
                    </select><br><br>
                </li>
                <li>
                    <label for="menu_deroulant_titre">Titre du menu (obligatoire) ? </label> 
                    <input type="text" name="menu_deroulant_titre" id="menu_deroulant_titre" placeholder="Ex : Menu 1" class="width100 text"  required="required">
                </li>
                <li>
                    <ul class="form_commandes">
                        <li>
                            <label for="nombre_de_sousmenus_deroulant">Nombre de sous-menus ?</label> 
                            <select name="nombre_de_sousmenus_deroulant" id="nombre_de_sousmenus_deroulant" data-elementname="menuderoulant_titre"  data-emplacement="ajoutSousMenus">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                            </select>
                        </li>
                        <li></li>
                        <li>
                            <label for="menu_deroulant_icon">Icône du menu:</label> 
                            <input type="text" name="menu_deroulant_icon" id="menu_deroulant_icon" class="width100 text" placeholder="Voir définition icones du DSFR">
                            <label>Consulter <a href="https://www.systeme-de-design.gouv.fr/elements-d-interface/fondamentaux-techniques/icone/" target="_blank" rel="external" titre="aller sur la page des icônes du DSFR - nouvelle fenêtre">les icônes</a> disponibles
        </label>
                        </li>
                    </ul>
                </li>
                <li class="gestion_ajoutSousMenus" style="display:grid;">
                    <ul class="form_configuration ajoutSousMenus_1" style="order:1;">
                        <li>
                            <label for="menuderoulant_titre_1">Titre sousmenu 1 (obligatoire):</label> 
                            <input type="text" name="menuderoulant_titre_1" id="menuderoulant_titre_1" placeholder="Ex : SousMenu 1" class="width100 text"  required="required">
                        </li>
                        <li>
                            <label for="menuderoulant_url_1">Url sous-menu 1 (obligatoire):</label> 
                            <input type="text" name="menuderoulant_url_1" id="menuderoulant_url_1" placeholder="Ex : https://lien_ssmenu1 ou /ssmenu1" class="width100 text" required="required">
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>';
?>