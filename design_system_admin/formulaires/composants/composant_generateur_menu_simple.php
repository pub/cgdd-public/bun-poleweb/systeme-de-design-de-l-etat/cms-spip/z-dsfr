
<?php
    function generateUuidV4() {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
    echo'
    <ul class="affichage_generateur">
        <li id="simple_menu_nav">
            <style>
                input.text, textarea, select {
                position: relative;
                z-index: 2;
                padding: var(--spip-form-input-padding-y) var(--spip-form-input-padding-x);
                margin: 0;
                width: 100%;
                border: 1px solid var(--spip-form-input-border-color);
                border-radius: var(--spip-form-input-border-radius);
                background-color: var(--spip-color-white);
                transition: box-shadow 0.1s;
            }
            .affichage_generateur{
                margin-top:1.5rem;
            }
            .dontshow{
                display: none !important;
            }
            input.button-primary {
                background: #2271b1;
                border-color: #2271b1;
                color: #fff;
                text-decoration: none;
                text-shadow: none;
                display: inline-block;
                font-size: 13px;
                line-height: 2.15384615;
                min-height: 30px;
                margin: 0;
                padding: 0 10px;
                cursor: pointer;
                border-width: 1px;
                border-style: solid;
                -webkit-appearance: none;
                border-radius: 3px;
                white-space: nowrap;
                box-sizing: border-box;
                width:80% !important;
            }

            
            .button-home {
                padding: 1rem;
                background-color: white;
                color: black;
                display: inline-block;
                text-decoration: none;
            }
            .button-home:hover {
                background-color: #f6f6f6;
            }
            a:active, a:hover {
                color: #135e96;
            }
            </style>
            <ul class="form_configuration resultat_menu_simple dontshow">
                
            </ul>
            <ul class="form_commandes">
                <li>
                    <label for="menu-simple-titre"><span class="redbold">*</span> Nom du menu:</label><br>
                    <input type="text" name="menu-simple-titre" id="menu-simple-titre" class="width100 text" placeholder="Ex: Accueil" autocomplete="off" autocomplete="off" required="required">
                </li>
                <li>
                    <label for="menu-simple-url"><span class="redbold">*</span> URL du menu:</label><br>
                    <input type="text" name="menu-simple-url" id="menu-simple-url" class="width100 text" placeholder="Ex: https://... ou /xxxxxx" autocomplete="off" required="required">
                </li>
                <li>
                    <label for="menu-simple-icon">icon du menu:</label><br>
                    <input type="text" name="menu-simple-icon" id="menu-simple-icon" class="width100 text" placeholder="Voir définition icones du DSFR" autocomplete="off">
                </li>
            </ul>
        </li>
    </ul>';
?>