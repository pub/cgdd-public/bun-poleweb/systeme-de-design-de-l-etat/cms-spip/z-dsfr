<?php
echo '
    <li class="_masquer_cookies gestion_masquer_cookies_select2options">
        <div class="editer editer_titre_texte_cookies obligatoire saisie_input editer_even">
            <label class="editer-label" for="champ_titre_texte_cookies">Titre du texte des cookies<span class="obligatoire"> (obligatoire)</span></label>
            <input type="text" name="titre_texte_cookies" class="text" id="champ_titre_texte_cookies" required="required" data-parent="masquer_cookies"  placeholder="Saisir le titre du panneau d\'informations des cookies">
        </div>
    </li>
    <li class="_masquer_cookies gestion_masquer_cookies_select2options">
        <div class="editer editer_texte_cookies obligatoire saisie_textarea editer_odd">
            <label class="editer-label" for="champ_texte_cookies">Texte des cookies<span class="obligatoire"> (obligatoire)</span></label>
            <textarea name="texte_cookies" class="" id="champ_texte_cookies" rows="6" cols="33" required="required" data-parent="masquer_cookies" placeholder="Saisir le texte du panneau d\'informations des cookies"></textarea>
        </div>
    </li>';  
?>