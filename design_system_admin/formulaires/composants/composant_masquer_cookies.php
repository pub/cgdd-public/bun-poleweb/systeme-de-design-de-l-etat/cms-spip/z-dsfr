<?php
echo '
    <li class="_masquer_cookies">
        <div class="editer editer_liste_des_cookies obligatoire saisie_textarea editer_even">
            <label class="editer-label" for="champ_liste_des_cookies">Liste des cookies?<span class="obligatoire"> (obligatoire)</span></label>
            <textarea name="liste_des_cookies" id="champ_liste_des_cookies" rows="4" cols="33" required="required" aria-describedby="explication_liste_des_cookies" data-parent="masquer_cookies" placeholder="Saisir votre liste de cookies."></textarea>
        </div>
    </li>
    <li class="_masquer_cookies">
        <div class="editer editer_modifier_texte_cookies saisie_selection editer_odd">
            <label class="editer-label" for="champ_modifier_texte_cookies">Modifier le texte des cookies?</label>
            <select name="modifier_texte_cookies" id="champ_modifier_texte_cookies" data-information="select2options" data-remove="non" data-parent="masquer_cookies" required="required">
                <option value="oui" selected="selected">oui</option>
                <option value="non">Non</option>
            </select>
        </div>
    </li>
    <li class="_masquer_cookies gestion_masquer_cookies_select2options">
        <div class="editer editer_titre_texte_cookies obligatoire saisie_input editer_even">
            <label class="editer-label" for="champ_titre_texte_cookies">Titre du texte des cookies<span class="obligatoire"> (obligatoire)</span></label>
            <input type="text" name="titre_texte_cookies" class="text" id="champ_titre_texte_cookies" required="required" aria-describedby="explication_titre_texte_cookies" data-parent="masquer_cookies" placeholder="Saisir le titre du panneau d\'informations des cookies">
        </div>
    </li>
    <li class="_masquer_cookies gestion_masquer_cookies_select2options">
        <div class="editer editer_texte_cookies obligatoire saisie_textarea editer_odd">
            <label class="editer-label" for="champ_texte_cookies">Texte des cookies<span class="obligatoire"> (obligatoire)</span></label>
            <textarea name="texte_cookies" id="champ_texte_cookies" rows="6" cols="33" required="required" aria-describedby="explication_texte_cookies" data-parent="masquer_cookies" placeholder="Saisir le texte du panneau d\'informations des cookies"></textarea>
        </div>
    </li>';  
?>