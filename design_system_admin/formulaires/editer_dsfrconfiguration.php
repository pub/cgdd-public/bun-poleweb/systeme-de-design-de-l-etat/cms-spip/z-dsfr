<?php

/**
 * Gestion du formulaire de d'édition de dsfrconfiguration
 *
 * @plugin     Design system Admin
 * @copyright  2023
 * @author     Mikaël Folio
 * @licence    GNU/GPL
 * @package    SPIP\Design_system_admin\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Déclaration des saisies de dsfrconfiguration
 *
 * @param int|string $id_dsfrconfiguration
 *     Identifiant du dsfrconfiguration. 'new' pour un nouveau dsfrconfiguration.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un dsfrconfiguration source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du dsfrconfiguration, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array[]
 *     Saisies du formulaire
 */
function formulaires_editer_dsfrconfiguration_saisies_dist($id_dsfrconfiguration = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$saisies = [
	];
	return $saisies;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_dsfrconfiguration
 *     Identifiant du dsfrconfiguration. 'new' pour un nouveau dsfrconfiguration.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un dsfrconfiguration source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du dsfrconfiguration, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_dsfrconfiguration_identifier_dist($id_dsfrconfiguration = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	return json_encode([intval($id_dsfrconfiguration)]);
}

/**
 * Chargement du formulaire d'édition de dsfrconfiguration
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_dsfrconfiguration
 *     Identifiant du dsfrconfiguration. 'new' pour un nouveau dsfrconfiguration.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un dsfrconfiguration source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du dsfrconfiguration, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_dsfrconfiguration_charger_dist($id_dsfrconfiguration = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$valeurs = formulaires_editer_objet_charger('dsfrconfiguration', $id_dsfrconfiguration, '', $lier_trad, $retour, $config_fonc, $row, $hidden);

    $args = func_get_args();
	$valeurs['saisies'] = formulaires_editer_dsfrconfiguration_saisies_dist(...$args);
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition de dsfrconfiguration
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_dsfrconfiguration
 *     Identifiant du dsfrconfiguration. 'new' pour un nouveau dsfrconfiguration.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un dsfrconfiguration source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du dsfrconfiguration, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_editer_dsfrconfiguration_verifier_dist($id_dsfrconfiguration = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {

	$erreurs = formulaires_editer_objet_verifier('dsfrconfiguration', $id_dsfrconfiguration);


	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de dsfrconfiguration
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_dsfrconfiguration
 *     Identifiant du dsfrconfiguration. 'new' pour un nouveau dsfrconfiguration.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un dsfrconfiguration source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du dsfrconfiguration, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 */
function formulaires_editer_dsfrconfiguration_traiter_dist($id_dsfrconfiguration = 'new', $retour = '', $lier_trad = 0, $config_fonc = '', $row = [], $hidden = '') {
	$retours = formulaires_editer_objet_traiter('dsfrconfiguration', $id_dsfrconfiguration, '', $lier_trad, $retour, $config_fonc, $row, $hidden);
	return $retours;
}
