<?php
// Activer les rapports d’erreurs PHP
// error_reporting(E_ALL^E_NOTICE);
// ini_set ("display_errors", "on");
 
// Afficher toutes les erreurs dans SPIP
// define('SPIP_ERREUR_REPORT', E_ALL);
if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

define('_SPIP_DEBUG', true);

// configuration des composants du plugin
// définir la position des composants de commandes
define('MASQUER_LE_TITRE', '1');
define('_MASQUER_LE_SLOGAN', '2');
define('_DRAPEAU_EN_BERNE', '3');
define('_MASQUER_HAUT_DE_PAGE', '4');
define('_HOME_EN_H1', '5');
define('_MASQUER_RECHERCHE', '6');
define('_MASQUER_AUTEUR', '7');
define('_MASQUER_CONTACT', '8');
define('_MASQUER_MESSAGE_ALERTE', '9');
define('_MASQUER_STATISTIQUES', '10');
define('_MASQUER_LISTE_DE_LIEN', '11');
define('_MASQUER_MENU', '12');
define('_MASQUER_PARTENAIRES', '13');
define('_MASQUER_COOKIES', '14');
define('_MASQUER_PARAMETRES_AFFICHAGE', '15');
define('_MASQUER_LISTE_ACCES_RAPIDE', '16');
define('_MASQUER_LA_NEWSLETTER', '17');
define('_MASQUER_PARTAGE_RESEAUX_SOCIAUX', '18');
define('_MASQUER_LOGO_SECONDAIRE', '19');
define('_MASQUER_LIENS_OBLIGATION_LEGALE', '20');
// futurs composants
//define('_COMPOSANT_18', 'order:18');
//define('_COMPOSANT_19', 'order:19');
//define('_COMPOSANT_20', 'order:20');
?>