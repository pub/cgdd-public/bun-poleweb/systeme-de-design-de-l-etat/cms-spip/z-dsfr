<?php

/**
 * Utilisation de l'action supprimer pour l'objet dsfrconfiguration
 *
 * @plugin     Design system Admin
 * @copyright  2023
 * @author     Mikaël Folio
 * @licence    GNU/GPL
 * @package    SPIP\Design_system_admin\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}



/**
 * Action pour supprimer un·e dsfrconfiguration
 *
 * Vérifier l'autorisation avant d'appeler l'action.
 *
 * @example
 *     ```
 *     [(#AUTORISER{supprimer, dsfrconfiguration, #ID_DSFRCONFIGURATION}|oui)
 *         [(#BOUTON_ACTION{<:dsfrconfiguration:supprimer_dsfrconfiguration:/>,
 *             #URL_ACTION_AUTEUR{supprimer_dsfrconfiguration, #ID_DSFRCONFIGURATION, #URL_ECRIRE{dsfrconfigurations}},
 *             danger, <:dsfrconfiguration:confirmer_supprimer_dsfrconfiguration:/>})]
 *     ]
 *     ```
 *
 * @example
 *     ```
 *     [(#AUTORISER{supprimer, dsfrconfiguration, #ID_DSFRCONFIGURATION}|oui)
 *         [(#BOUTON_ACTION{
 *             [(#CHEMIN_IMAGE{dsfrconfiguration-del-24.png}|balise_img{<:dsfrconfiguration:supprimer_dsfrconfiguration:/>}|concat{' ',#VAL{<:dsfrconfiguration:supprimer_dsfrconfiguration:/>}|wrap{<b>}}|trim)],
 *             #URL_ACTION_AUTEUR{supprimer_dsfrconfiguration, #ID_DSFRCONFIGURATION, #URL_ECRIRE{dsfrconfigurations}},
 *             icone s24 horizontale danger dsfrconfiguration-del-24, <:dsfrconfiguration:confirmer_supprimer_dsfrconfiguration:/>})]
 *     ]
 *     ```
 *
 * @example
 *     ```
 *     if (autoriser('supprimer', 'dsfrconfiguration', $id_dsfrconfiguration)) {
 *          $supprimer_dsfrconfiguration = charger_fonction('supprimer_dsfrconfiguration', 'action');
 *          $supprimer_dsfrconfiguration($id_dsfrconfiguration);
 *     }
 *     ```
 *
 * @param null|int $arg
 *     Identifiant à supprimer.
 *     En absence de id utilise l'argument de l'action sécurisée.
**/
function action_supprimer_dsfrconfiguration_dist($arg = null) {
	$need_confirm = false;
	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
		$need_confirm = true;
	}
	$arg = intval($arg);

	if ($need_confirm) {
		$ok = confirmer_supprimer_dsfrconfiguration_avant_action(_T('dsfrconfiguration:confirmer_supprimer_dsfrconfiguration'), _T('item_oui') . '! ' . _T('dsfrconfiguration:supprimer_dsfrconfiguration'));
	}

	// cas suppression
	if (autoriser('supprimer', 'dsfrconfiguration', $arg)) {
		if ($arg) {
			$objet = sql_fetsel('*', 'spip_dsfrconfigurations', 'id_dsfrconfiguration=' . sql_quote($arg));
			$qui = (!empty($GLOBALS['visiteur_session']['id_auteur']) ? 'auteur #' . $GLOBALS['visiteur_session']['id_auteur'] : 'IP ' . $GLOBALS['ip']);
			spip_log("SUPPRESSION dsfrconfiguration#$arg par $qui : " . json_encode($objet), 'suppressions' . _LOG_INFO_IMPORTANTE);

			sql_delete('spip_dsfrconfigurations', 'id_dsfrconfiguration=' . sql_quote($arg));
			include_spip('action/editer_logo');
			logo_supprimer('spip_dsfrconfigurations', $arg, 'on');
			logo_supprimer('spip_dsfrconfigurations', $arg, 'off');

			// invalider le cache
			include_spip('inc/invalideur');
			suivre_invalideur("id='dsfrconfiguration/$arg'");
		}
		else {
			spip_log("action_supprimer_dsfrconfiguration_dist $arg pas compris");
		}
	}
}

/**
 * Confirmer avant suppression si on arrive par un bouton action
 * @param string $titre
 * @param string $titre_bouton
 * @param string|null $url_action
 * @return bool
 */
function confirmer_supprimer_dsfrconfiguration_avant_action($titre, $titre_bouton, $url_action = null) {

	if (!$url_action) {
		$url_action = self();
		$action = _request('action');
		$url_action = parametre_url($url_action, 'action', $action, '&');
	}
	else {
		$action = parametre_url($url_action, 'action');
	}
	$arg = parametre_url($url_action, 'arg');
	$confirm = md5("$action:$arg:" . realpath(__FILE__));
	if (_request('confirm_action') === $confirm) {
		return true;
	}

	$url_confirm = parametre_url($url_action, 'confirm_action', $confirm, '&');
	include_spip('inc/filtres');
	$bouton_action = bouton_action($titre_bouton, $url_confirm);
	$corps = "<div style='text-align:center;'>$bouton_action</div>";

	include_spip('inc/minipres');
	echo minipres($titre, $corps);
	exit;
}
