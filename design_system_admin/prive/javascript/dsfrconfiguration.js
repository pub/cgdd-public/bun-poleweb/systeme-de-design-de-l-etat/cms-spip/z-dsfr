// Exécute le code après le chargement complet du document
jQuery(document).ready(function($) {

    var pluginName = 'design_system_admin'; // Remplacez par le nom du plugin
    var filename = 'paquet.xml'; // Remplacez par le nom du fichier
    var fileName_plugin_dist = '/plugins-dist/z-dsfr/' + pluginName +'/' + filename; 
    var fileName_plugin = '/plugins/auto/z-dsfr/' + pluginName +'/' + filename; 
    var chemin_plugin = '/plugins-dist/'; // Chemin par défaut
    $.get(fileName_plugin_dist)
    .done(function() {
        // Le fichier a été trouvé dans le dossier 'plugins'
        console.log('File found in plugins-dist folder');
        chemin_plugin = '/plugins-dist/';
    })
    .fail(function() {
        // Le fichier n'a pas été trouvé dans le dossier 'plugins', essayons 'plugins-dist'
        $.get(fileName_plugin)
            .done(function() {
                // Le fichier a été trouvé dans le dossier 'plugins-dist'
                console.log('File found in plugins folder');
                chemin_plugin = '/plugins/auto/';
            })
            .fail(function() {
                // Le fichier n'a pas été trouvé dans le dossier 'plugins-dist' non plus
                console.log('File not found in either folder');
            });
    });
    

    // Configuration initiale
    supprimerElementsInutiles();
    configurerElements();

    // Écoute les changements
    surveillerChangementsCheckbox();
    surveillerSelect2Options();
    surveillerSelectNumeric();
    
    initialiserElements();
    surveillerDemandeGenerateurs();


    // Supprime les éléments indésirables
    function supprimerElementsInutiles() {
        $("#navigation, #extra").remove();
    }

    // Configure les éléments
    function configurerElements() {
        $('[data-information="commandes"]').each(function() {
            const nomCheckbox = $(this).attr('name');
            const elementCache = $('[name="_' + nomCheckbox + '"]');
            const elementMasque = $('._' + nomCheckbox);

            assurerAttributData('data-gestion', 'masquer_le_titre', 'true');
            assurerAttributData('data-gestion', 'masquer_le_slogan', 'true');
            assurerAttributData('data-gestion', 'masquer_message_alerte', 'true');
            assurerAttributData('data-gestion', 'masquer_statistiques', 'true');
            assurerAttributData('data-gestion', 'masquer_partage_reseaux_sociaux', 'true');
            assurerAttributData('data-gestion', 'masquer_la_newsletter', 'true');
            assurerAttributData('data-gestion', 'masquer_parametres_affichage', 'true');
            assurerAttributData('data-gestion', 'masquer_cookies', 'true');
            assurerAttributData('data-gestion', 'masquer_liste_acces_rapide', 'true');
            assurerAttributData('data-gestion', 'masquer_logo_secondaire', 'true');
            assurerAttributData('data-gestion', 'masquer_partenaires', 'true');
            assurerAttributData('data-gestion', 'masquer_menu', 'true');
            assurerAttributData('data-gestion', 'masquer_langue', 'true');
            assurerAttributData('data-gestion', 'masquer_liste_de_lien', 'true');
            assurerAttributData('data-gestion', 'masquer_liens_obligation_legale', 'true');
            assurerAttributData('data-gestion', 'masquer_auteur', 'true');
            assurerAttributData('data-gestion', 'masquer_contact', 'true');
    
            assurerAttributData('data-required', 'lien_dacces_rapide1', 'non');
            assurerAttributData('data-required', 'lien_dacces_rapide2', 'non');
            assurerAttributData('data-required', 'lien_dacces_rapide3', 'non');

            assurerAttributData('data-elementname', 'nombre_de_liste_de_liens', 'liste_de_liens');        
            assurerAttributData('data-elementname', 'nombre_de_menu', 'menu');
            assurerAttributData('data-elementname', 'nombre_de_langue', 'langue');
            assurerAttributData('data-elementname', 'nombre_de_partenaires_principaux', 'partenaires_principaux');
            assurerAttributData('data-elementname', 'nombre_de_partenaires_secondaires', 'partenaires_secondaires');
            assurerAttributData('data-elementname', 'nombre_de_liens_ecosysteme', 'lien_eco');
            assurerAttributData('data-elementname', 'nombre_de_liens_obligations_legales', 'lien_legal');
            
            assurerAttributData('data-information', 'type_formulaire_newsletter', 'select2options');
            assurerAttributData('data-information', 'modifier_texte_cookies', 'select2options');
            
            assurerAttributData('data-remove', 'type_formulaire_newsletter', 'lien');
            assurerAttributData('data-remove', 'modifier_texte_cookies', 'non');

            assurerAttributData('data-parent', 'type_formulaire_newsletter', 'masquer_la_newsletter');
            assurerAttributData('data-parent', 'message_alerte', 'masquer_message_alerte');
            assurerAttributData('data-parent', 'contact_auteur', 'masquer_auteur');
            assurerAttributData('data-parent', 'contact_contact', 'masquer_contact');
            assurerAttributData('data-parent', 'script_statistiques', 'masquer_statistiques');
            assurerAttributData('data-parent', 'liste_des_cookies', 'masquer_cookies');
            assurerAttributData('data-parent', 'texte_cookies', 'masquer_cookies');
            assurerAttributData('data-parent', 'modifier_texte_cookies', 'masquer_cookies');
            assurerAttributData('data-parent', 'nombre_de_liste_de_liens', 'masquer_liste_de_lien');
            assurerAttributData('data-parent', 'nombre_de_menu', 'masquer_menu');
            assurerAttributData('data-parent', 'nombre_de_langue', 'masquer_langue');
            assurerAttributData('data-parent', 'nombre_de_partenaires_principaux', 'partenaires_principaux');
            assurerAttributData('data-parent', 'nombre_de_partenaires_secondaires', 'partenaires_secondaires');
            assurerAttributData('data-parent', 'nombre_de_liens_ecosysteme', 'nombre_de_liens_ecosysteme');
            assurerAttributData('data-parent', 'nombre_de_liens_obligations_legales', 'nombre_de_liens_obligations_legales');

    
            assurerAttributData('data-select', 'nombre_de_liste_de_liens', 'numeric');        
            assurerAttributData('data-select-0', 'nombre_de_liste_de_liens', '1');        
            assurerAttributData('data-select', 'nombre_de_menu', 'numeric');
            assurerAttributData('data-select-0', 'nombre_de_menu', '1');
            assurerAttributData('data-select-0', 'nombre_de_langue', '1');
            assurerAttributData('data-select', 'nombre_de_partenaires_principaux', 'numeric');
            assurerAttributData('data-select', 'nombre_de_partenaires_secondaires', 'numeric');
            assurerAttributData('data-select', 'nombre_de_liens_ecosysteme', 'numeric');
            assurerAttributData('data-select', 'nombre_de_liens_obligations_legales', 'numeric');
            assurerAttributData('data-select-0', 'nombre_de_partenaires_principaux', '0');
            assurerAttributData('data-select-0', 'nombre_de_partenaires_secondaires', '0');
            assurerAttributData('data-select-0', 'nombre_de_liens_ecosysteme', '1');
            assurerAttributData('data-select-0', 'nombre_de_liens_obligations_legales', '1');

            //if (doitAjouterNouveauxLiens('ajout_liens_liste_de_lien')) { ajouterNouveauxLiens('ajout_liens_liste_de_lien','nombre_de_liste_de_liens','ldl','liste_de_lien');}
            //if (doitAjouterNouveauxLiens('ajout_liens_liste_de_menu')) { ajouterNouveauxLiens('ajout_liens_liste_de_menu','nombre_de_menu','menutheme', 'menu');}

            mettreAJourVisibiliteElement($(this), elementCache, elementMasque, '', false);
        });
    }

    // Assure que l'attribut 'data' est présent
    function assurerAttributData(typeData, nom, valeur) {
        const elementParent = $('[name="' + nom + '"]');
        if (!elementParent.attr(typeData)) {
            elementParent.attr(typeData, valeur); 
        }
    }

    // Fonction pour vérifier si de nouveaux liens doivent être ajoutés
    function doitAjouterNouveauxLiens(name) {
        // Si l'élément avec ce nom n'existe pas, renvoie vrai
        return $(`[name="${name}"]`).length === 0;
    }
    // Fonction pour créer une requête AJAX
    function createAjaxRequest(filename, data, emplacement) {
        return new Promise(function(resolve, reject) {
        $.ajax({
            url: filename,
            type: 'GET',
            data: data,
            success: function(response) {
            $(emplacement).append(response);
            resolve(); // Résoudre la promesse une fois que la requête AJAX est terminée
            },
            error: function() {
            console.error('Une erreur s\'est produite lors de l\'ajout de la div.');
            reject(); // Rejeter la promesse en cas d'erreur
            }
        });
        });
    }
  
    function processFilesSequentially(fileList) {
        var sequence = Promise.resolve();
    
        fileList.forEach(function(file) {
        sequence = sequence.then(function() {
            return createAjaxRequest(file.filename, file.data, file.emplacement);
        });
        });
        return sequence;
    }
    // Cette fonction est utilisée pour construire les champs du formulaire
    function construireLesChampsDuFormulaire(elementMenu, elementCache, elementMasque, nomCheckbox, autreParametre = '') {
        let fichierURL =  chemin_plugin + `z-dsfr/design_system_admin/formulaires/composants/composant_${nomCheckbox}.php`;
        let emplacementInsertion = $(`.gestion_${elementCache}`);
        let requestData = {
            masquer_parametres_affichage: $(`[name="_masquer_parametres_affichage"]`).val(),
            masquer_liste_acces_rapide: $(`[name="_masquer_liste_acces_rapide"]`).val(),
            autreparametre: autreParametre,
            chemin_plugin: chemin_plugin
        };

        createAjaxRequest(fichierURL, requestData, emplacementInsertion);
    }



    // Cette fonction modifie la visibilité d'un élément en fonction de l'état d'une checkbox
    function mettreAJourVisibiliteElement(checkbox, elementCache, elementMasque, nomCheckbox, onChange = false) {
        // Récupère l'état de la checkbox
        const estCoche = checkbox.is(':checked');

        // Modifie la valeur de l'élément caché et de la checkbox en fonction de l'état de la checkbox
        elementCache.val(estCoche ? 'oui' : 'non');
        checkbox.val(estCoche ? 'oui' : 'non');

        // Modifie la visibilité de l'élément masqué en fonction de l'état de la checkbox
        estCoche ? elementMasque.hide() : elementMasque.show();

        // Récupère tous les éléments qui ont pour parent la checkbox et modifie leur attribut 'required' en fonction de l'état de la checkbox
        const elementsAssocies = $(`[data-parent="${checkbox.attr('name')}"]`);
        estCoche ? elementsAssocies.removeAttr('required') : elementsAssocies.attr('required', 'required');

        // Si l'élément caché a pour valeur 'non' et que le paramètre onChange est vrai, alors appelle la fonction construireLesChampsDuFormulaire
        if(elementCache.val() === 'non' && onChange){
            construireLesChampsDuFormulaire(checkbox, nomCheckbox, elementMasque, nomCheckbox);
        }

        // Si le nom de la checkbox est 'masquer_parametres_affichage', alors on ajuste les paramètres d'affichage
        if(nomCheckbox === 'masquer_parametres_affichage') {
            const value = elementCache.val();
            let emplacement = 'masquer_liste_acces_rapide';
            value === 'non' ? ajusterParametresAffichageNon(emplacement, nomCheckbox) : ajusterParametresAffichageOui(emplacement);
        }
    }

    // Cette fonction ajuste les paramètres d'affichage quand la valeur de l'élément caché est 'non'
    function ajusterParametresAffichageNon(emplacement, nomCheckbox) {
        $('[name="masquer_liste_acces_rapide"]').val('non').removeAttr('checked').attr("disabled","disabled");
        $('[name="_masquer_liste_acces_rapide"]').val('non');
        $(`.remove_width_${nomCheckbox}`).remove();
        doitAjouterNouveauxLiens('lien_dacces_rapide1') && construireLesChampsDuFormulaire('non', emplacement, undefined, 'masquer_liste_acces_rapide');
    }

    // Cette fonction ajuste les paramètres d'affichage quand la valeur de l'élément caché est 'oui'
    function ajusterParametresAffichageOui(emplacement) {
        $('[name="masquer_liste_acces_rapide"]').removeAttr("disabled");
        const elementDelete = $(".remove_width_masquer_parametres_affichage");

        if ($('[name="masquer_liste_acces_rapide"]').val() === 'non') {
            construireLesChampsDuFormulaire('non', emplacement, elementDelete, 'build_mlar_width_masquer_parametres_affichage');
        } else {
            construireLesChampsDuFormulaire('non', emplacement, elementDelete, 'composant_masquer_liste_acces_rapide');
        }
    }


    // Cette fonction surveille les changements d'état des éléments select ayant 2 options
    function surveillerSelect2Options() {
        $(document).on('change', '[data-information="select2options"]', function() {
            // Extraction des attributs nécessaires de l'élément select
            const nomSelect = $(this).attr('name'); 
            const parentElement = $(this).attr('data-parent');
            const nomComposant = `${parentElement}_select2options`;
            const triggerRemove = $(this).attr('data-remove');
            const elementValeur = $(this).val();
            const elementCache = $(`.${parentElement}_select2options`);

            // Suppression du composant si la valeur de l'élément est égale à triggerRemove
            if(elementValeur === triggerRemove) {
                $(`.gestion_${nomComposant}`).remove();
            } 
            // Construction des champs du formulaire dans le cas contraire
            else {
                construireLesChampsDuFormulaire($(this), parentElement, elementCache, nomComposant);
            }
        });
    }

     

    // Événement déclenché lorsqu'un changement est apporté à l'élément avec le nom 'nombre_de_sousmenus_deroulant'
    $(document).on('change', '[name="nombre_de_sousmenus_deroulant"]', function() {
        const nombre_de_sousmenus_deroulant = $(this).val();
        const nomElements = $(this).attr('data-elementname');
        const emplacementAjout = ".gestion_" + $(this).attr('data-emplacement');
        const fileNameElement =  chemin_plugin + `/z-dsfr/design_system_admin/formulaires/composants/composant_generateur_menu_deroulant_${nomElements}.php`;    
        var files = [];
        // Parcours du nombre de sous-menus déroulants
        for(let i = 1; i <= nombre_de_sousmenus_deroulant; i++){
            const nouvelElement = doitAjouterNouveauxLiens(`${nomElements}_${i}`); 
            if(nouvelElement){
                var dataLink = {
                    autreparametre: i,
                    chemin_plugin: chemin_plugin
                };                
                files.push({ filename: fileNameElement, data: dataLink, emplacement: emplacementAjout });
                // Si un nouvel élément doit être ajouté, construit le champ de formulaire et l'ajoute
                //construireLesChampsDuFormulaire($(this), emplacementAjout, '',  fileNameElement, i);
            }
        }
        processFilesSequentially(files);

        const selectCourant = $(this).attr('name');
        const nombreOptionsSelect = $(`[name="${selectCourant}"] option`).length;
        
        // Parcours du nombre d'options dans le select courant
        for(let j = parseInt(nombre_de_sousmenus_deroulant) + 1; j <= nombreOptionsSelect; j++){
            // Supprime l'élément
            const suppressionElement = `${emplacementAjout}_${j}`;
            $(`.${suppressionElement}`).remove();
        }
    });

    // Surveille les changements de l'état des checkboxes
    function surveillerChangementsCheckbox() {
        $('[data-information="commandes"]').on('click', function() {
            const nomCheckbox = $(this).attr('name');
            const gestion = $(this).attr('data-gestion') === 'true';
            console.log(`gestion: ${gestion}`);
            
            const elementCache = $(`[name="_${nomCheckbox}"]`);
            const elementMasque = $(`._${nomCheckbox}`);
            elementMasque.remove();

            mettreAJourVisibiliteElement($(this), elementCache, elementMasque, nomCheckbox, gestion);
        });
    }

    function surveillerSelectNumeric() {
        // Attachement de l'événement 'change' à l'élément avec l'attribut 'data-select' égal à 'numeric'
        $(document).on('change', '[data-select="numeric"]', function() {        
            var $this = $(this);
            var nombreElements = $this.val();
    
            const nomElements = $this.attr('data-elementname');
            const masqueElement = $this.attr('data-parent');
            const urlLink = $('[name="masquer_partenaires"]').data('link');
            const filenameElement =  chemin_plugin + `/z-dsfr/design_system_admin/formulaires/composants/composant_${nomElements}_numeric.php`;
            // Parcours et ajout des nouveaux éléments si nécessaire
            var files = []
            for(let i = 1; i <= nombreElements; i++){
                const nouvelElement = doitAjouterNouveauxLiens(`${nomElements}_${i}`); 
                if(nouvelElement){
                  var dataLink = {autreparametre: i,
                                  datalink: urlLink,
                                  chemin_plugin: chemin_plugin
                                };
                  const emplacement = `.gestion_${nomElements}`;
                  files.push({ filename: filenameElement, data: dataLink, emplacement: emplacement });
                  //createAjaxRequest(filenameElement, dataLink, emplacement);
                }
            }
            processFilesSequentially(files);
    
            // Suppression des éléments excédentaires
            const selectCourant = $this.attr('name');
            const nombreOptionsSelect = $(`[name="${selectCourant}"] option`).length;
            const dataSelect0 = $this.data('select-0');
            for(let j = parseInt(nombreElements) + 1; j <= nombreOptionsSelect; j++){
                const suppressionElement = `${masqueElement}_${j}`;
                $(`.${suppressionElement}`).remove();
            }
        });
    }

    // Paramètres d'initialisation 
    function initialiserElements() {
        const MasquerParametres = $('[name="masquer_parametres_affichage"]').val();
        if (MasquerParametres === 'non'){
            $('[name="masquer_liste_acces_rapide"]').val('non');
            $('[name="_masquer_liste_acces_rapide"]').val('non');
            $('[name="masquer_liste_acces_rapide"]').attr('disabled','disabled');
        }
    }

    window.onerror = function(message, source, lineno, colno, error) {
        if (message === "Uncaught TypeError: jQuery.spip is undefined") {
        return true; // Ignorer l'erreur
        }
        
    };
        
    function resetMenuGeneratorForm() {
        $('.affichage_generateur').remove();
        $('[name="submit_generateurs_menu"]').addClass('dontshow');
        $('[name="type-menu-type"]').val('');
        $('.les-resultats').addClass('dontshow');
        $('.les-resultats nav').remove();
        $('#modal').addClass('dontshow');
        $('[name="submit_generateurs_menu"]').attr("value", "Générer l'élément");
        $('button.autoriser_reinit_form').addClass('dontshow');        
        $('#resultats').remove();
        $('submit_generateurs_menu').addClass('dontshow');
    }
    function surveillerDemandeGenerateurs(){
        $(document).on('click', '.configurer_element', function(e) {
            e.preventDefault();
            $this = $(this);
            resetMenuGeneratorForm();
            const componentFille = $this.data('fille');
            if (!($this.data('fille'))) {return false;}
            const typeGenerateur = $this.data('type-generateur');
            const cheminElementFichier = $('[name="plugin_root"]').val();
            const fichierElement =  chemin_plugin + `z-dsfr/design_system_admin/formulaires/excerpt/excerpt-generateurs-${typeGenerateur}.php`;
            const emplacementElement = '.elementFormUl';
            const datalink = {typeGenerateur: typeGenerateur,
                              componentFille: componentFille,
                              chemin_plugin: chemin_plugin
                            };
            $('#modal').attr('data-parent', componentFille);
            $('#modal').removeClass('dontshow');
            $('#menu_generator input[type="submit"]').removeClass('dontshow');
            $('.simple_icone_flag_remove').remove();
            $(emplacementElement).append('<li class="simple_icone_flag_remove"><label>Type de menu: <span class="type-menu-type">' + typeGenerateur + '</span></label></li>');
            createAjaxRequest(fichierElement, datalink, emplacementElement);
      
          });

        $(document).on('change', '[name="type-menu-type"]', function() {
            // Suppression de l'élément ayant la classe 'affichage_generateur'
            $('.affichage_generateur').remove();
        
            // Gestion de la visibilité du bouton 'submit_generateurs_menu'
            if($(this).val() === '') {
                $('[name="submit_generateurs_menu"]').addClass('dontshow');
            } else {
                $('[name="submit_generateurs_menu"]').removeClass('dontshow');
            }
        
            // Génération d'une variable aléatoire unique
            const uniqueRandomVar = Math.random().toString(36).substr(2) + Date.now().toString(36);
        
            // Récupération des informations du select
            const OptionUtilisee = $(this).val();
            const selectedText = $(this).find("option:selected").text();
        
            // Localisation de l'emplacement où le générateur sera ajouté
            let emplacementGenerateurs = $(this).attr('data-emplacement');
            emplacementGenerateurs = $(`#${emplacementGenerateurs}`);
        
            // Construction de l'URL du composant à charger
            const filenameComponent =  chemin_plugin + `z-dsfr/design_system_admin/formulaires/composants/composant_generateur_${OptionUtilisee}.php?${uniqueRandomVar}`;
            
            // Requête AJAX pour charger le composant
            createAjaxRequest(filenameComponent, {
                autreParametre : selectedText
            }, emplacementGenerateurs);
            
        
            // Affichage du bouton de soumission
            $('.submit_generateurs_menu').removeClass('dontshow');       
        });
        function processListeDeLiens(typeMenuType, submitButton, elementParent, emplacement, filename) {     
            var nombre_de_liens_dans_liste = $('[name="nombre_de_liens_dans_liste"]').val();
            emplacement = '.resultat_liste_de_lien li nav';
            if(submitButton.attr("value") !== "Valider le résultat"){
            var mesdatas = {
                nombre_de_liens_dans_liste: nombre_de_liens_dans_liste,
                liste_de_liens_titre: $('[name="liste_de_liens_titre"]').val(),
                liste_de_liens_entete: $('[name="liste_de_liens_entete"]').val(),
                liste_de_liens_icon: $('[name="liste_de_liens_icon"]').val(),
                chemin_plugin: chemin_plugin
            };
        
            for(let i = 1; i <= nombre_de_liens_dans_liste; i++){
                mesdatas['liste_de_liens_titre_' + i] = $('[name="liste_de_liens_titre_' + i + '"]').val();
                mesdatas['liste_de_liens_url_' + i] = $('[name="liste_de_liens_url_' + i + '"]').val();
            }
            createAjaxRequest(filename, mesdatas, emplacement);
            submitButton.attr("value", "Valider le résultat");
            $('.les-resultats').removeClass('dontshow');
            } else {
                var resultat = createListeDeLienResult(nombre_de_liens_dans_liste);
                $('[name="' + elementParent + '"]').val(resultat);
                resetMenuGeneratorForm();
                submitButton.attr("value", "Valider le résultat");
                $('button.autoriser_reinit_form').removeClass('dontshow');
                $('.les-resultats').removeClass('dontshow');
            }
        }
        function createListeDeLienResult(nombreDeSousMenus) {
            var resultat = $('[name="liste_de_liens_titre"]').val() + ';' + $('[name="liste_de_liens_entete"]').val() + ';';
            for(let i = 1; i <= nombreDeSousMenus; i++){
                resultat += $('[name="liste_de_liens_titre_' + i + '"]').val() + ';' + $('[name="liste_de_liens_url_' + i + '"]').val();
                if(i < nombreDeSousMenus){
                    resultat += ';';
                }
            }
            return resultat;
          }
        $(document).on('click', '[name="submit_generateurs_menu"]', function(e) {
            e.preventDefault();            
            var form = document.getElementById('menu_generator');
            if (!form.checkValidity()) {
                $('#menu_generator').find('input[required]:visible, select[required]:visible, textarea[required]:visible').each(function() {
                    $(this).removeClass('validitynone');
                    if ($(this).val() === '') {
                        isValid = false;
                        $(this).focus();
                        $(this).attr('placeholder', 'Ce champ est obligatoire !!!');
                        $(this).addClass('validitynone');
                        return false;
                    } 
                });
                return;
            }

            $this = $(this);
            const submitButton = $('[name="submit_generateurs_menu"]');
            const typeGenerateur = $('[name="type_generateur"]').val();
            const elementParent = $('[name="element_parent"]').val();
            const emplacement = '.resultat_menu_simple fieldset';
    
            $('.resultat_menu_simple fieldset nav').remove();
            var uniqueRandomVar = Math.random().toString(36).substr(2) + Date.now().toString(36);
            var filename =  chemin_plugin + `z-dsfr/design_system_admin/formulaires/composants/composant_generateur_${typeGenerateur}_resultat.php?${uniqueRandomVar}`;
            switch (typeGenerateur) {
            case 'simple-icone':
                processMenuSimple(typeGenerateur, submitButton, elementParent, emplacement, filename);
            break;        
            case 'simple-image':
                processMenuSimple(typeGenerateur, submitButton, elementParent, emplacement, filename);
            break;        
            case 'menu':
                const list_menu_type = $('[name="list-menu-type"]').val()
                if (list_menu_type === 'menu_deroulant') {
                    processMenuDeroulant(typeGenerateur, submitButton, elementParent, '.les-resultats', 'composant_generateur_menu_deroulant_resultat');
                } else if (list_menu_type === 'megamenu') {
                    processMegaMenu(typeGenerateur, submitButton, elementParent, emplacement, filename);
                    } else {
                    processMenuSimple(typeGenerateur, submitButton, elementParent, emplacement, filename);
                    }
            break;        
            case 'liste_de_liens':
                processListeDeLiens(typeGenerateur, submitButton, elementParent, emplacement, filename);
            break;     
            default:
            break;
            }
        });
        $(document).on('change', '[name="list-menu-type"]', function() {
            // Suppression de l'élément ayant la classe 'affichage_generateur'
            $('.affichage_generateur').remove();
            $this = $(this);
            const type_generateur = $this.data('type_generateur');
            const element_parent = $this.data('element_parent');
            // Gestion de la visibilité du bouton 'submit_generateurs_menu'
            if($(this).val() === '') {
                $('[name="submit_generateurs_menu"]').addClass('dontshow');
            } else {
                $('[name="submit_generateurs_menu"]').removeClass('dontshow');
            }
        
            // Génération d'une variable aléatoire unique
            const uniqueRandomVar = Math.random().toString(36).substr(2) + Date.now().toString(36);
        
            // Récupération des informations du select
            const OptionUtilisee = $(this).val();
            const selectedText = $(this).find("option:selected").text();
        
            // Localisation de l'emplacement où le générateur sera ajouté
            let emplacementGenerateurs = '.' + $(this).attr('data-emplacement');
         
        
            // Construction de l'URL du composant à charger
            const filenameComponent =  chemin_plugin + `z-dsfr/design_system_admin/formulaires/composants/composant_generateur_${OptionUtilisee}.php?${uniqueRandomVar}`;
            
            // Requête AJAX pour charger le composant
            createAjaxRequest(filenameComponent, {
                autreParametre : selectedText,
                type_generateur: type_generateur,
                element_parent: element_parent
            }, emplacementGenerateurs);
            
        
            // Affichage du bouton de soumission
            $('.submit_generateurs_menu').removeClass('dontshow');       
        });
        function processMenuSimple(typeMenuType, submitButton, elementParent, emplacement, filename) {
            var menuSimpleTitre = $('[name="menu-simple-titre"]').val();
            var menuSimpleUrl = $('[name="menu-simple-url"]').val();
            var menuSimpleIcon = $('[name="menu-simple-icon"]').val();
            var isValid = true;
            $('#menu_generator').find('input[required]:visible, select[required]:visible, textarea[required]:visible').each(function() {
                if ($(this).val() === '') {
                    isValid = false;
                    $(this).focus();
                    $(this).attr('placeholder', 'Ce champ est obligatoire');
                    return false;
                } 
            });
            if (isValid === true) {
                var resultat = 'direct;' + menuSimpleTitre + ';' + menuSimpleUrl + ';' + menuSimpleIcon;
                $('[name="' + elementParent + '"]').val(resultat);
                resetMenuGeneratorForm();
            }
        }    
        $(document).on('change', '[name="nombre_de_liens_dans_liste"]', function() {
            const nombre_de_liens_dans_liste = $(this).val();
            const nomElements = $(this).attr('data-elementname');
            const emplacementAjout = $(this).attr('data-emplacement');
            const fileNameElement =  chemin_plugin + `z-dsfr/design_system_admin/formulaires/composants/composant_generateur_${nomElements}.php`; 
            const typeGenerateur = $('[name="type_generateur"]').val();
            const elementParent = $('[name="element_parent"]').val();
          
            var files = []
            for(let i = 1; i <= nombre_de_liens_dans_liste; i++){
                const nouvelElement = doitAjouterNouveauxLiens(`${nomElements}_titre_${i}`); 
                if(nouvelElement){
                  var dataLink = {autreparametre: i,
                    nombre_de_liens_dans_liste: nombre_de_liens_dans_liste,
                    chemin_plugin: chemin_plugin
                                };
                  const emplacement = `.gestion_${emplacementAjout}`;
                  files.push({ filename: fileNameElement, data: dataLink, emplacement: emplacement });
                  //createAjaxRequest(filenameElement, dataLink, emplacement);
                }
            }
            processFilesSequentially(files);
          
          
            const selectCourant = $(this).attr('name');
            const nombreOptionsSelect = $(`[name="${selectCourant}"] option`).length;
            
            // Parcours du nombre d'options dans le select courant
            for(let j = parseInt(nombre_de_liens_dans_liste) + 1; j <= nombreOptionsSelect; j++){
                // Supprime l'élément
                const suppressionElement = `${emplacementAjout}_${j}`;
                $(`.${suppressionElement}`).remove();
            }
          });
          
        function processMenuDeroulant(typeMenuType, submitButton, elementParent, emplacement, filename) {
            var nombreDeSousMenus = $('[name="nombre_de_sousmenus_deroulant"]').val();

                var resultat = createMenuDeroulantResult(nombreDeSousMenus);
                $('[name="' + elementParent + '"]').val(resultat);
                resetMenuGeneratorForm();
                submitButton.attr("value", "Valider le résultat");
                $('button.autoriser_reinit_form').removeClass('dontshow');
                $('.les-resultats').removeClass('dontshow');
        }

        function processMegaMenu(typeMenuType, submitButton, elementParent, emplacement, filename) {        
            var nombreDeBlocDeMenus = $('[name="nombre_de_menus_megamenu"]').val();
            
                //megamenu;3;Mega;/IMG/theme.svg;titre;h2;desc;cat;urlcat;menu1b1;11;11;21;21;menu2b2;12;12;menu3b3;13;13;23;23;33;33;
                var resultat = 'megamenu;' + nombreDeBlocDeMenus + ';' + $('[name="menu-megamenu-titre"]').val() + ';' + $('[name="menu-megamenu-image"]').val() + ';' + $('[name="contenu-megamenu-titre"]').val() + ';' + $('[name="entete_titre_megamenu"]').val() + ';' + $('[name="menu-megamenu-description"]').val() + ';' + $('[name="categorie-megamenu-titre"]').val() + ';' + $('[name="categorie-megamenu-url"]').val() + ';'
                for(var i =1; i <= nombreDeBlocDeMenus; i++ ){
                    resultat += 'menu' + i + ';' + $('[name="menu-megamenu-titre-menu-bloc' + i + '"]').val() + ';';
                    var nombreDeSousMenus = $('[name="nombre_de_menus_megamenu_bloc' + i + '"]').val();
                    for(var j = 1; j <= nombreDeSousMenus; j++){
                        resultat += $('[name="titre' + j + 'menu' + i + '"]').val() + ';' + $('[name="url' + j + 'menu' + i + '"]').val();
                        if (j <= nombreDeSousMenus){
                            resultat += ';';
                        }
                    }                
                }
                $('[name="' + elementParent + '"]').val(resultat);
                resetMenuGeneratorForm();
                submitButton.attr("value", "Valider le résultat");
                $('button.autoriser_reinit_form').removeClass('dontshow');
                $('.les-resultats').removeClass('dontshow');
                
        }
        $(document).on('change', '[name="nombre_de_menus_megamenu"]', function() {
            const $this = $(this)
            const menuType = $this.data('menutype');
            const nombreBlocMega = $this.val();
            const nomElements = $this.data('emplacement');
            const emplacementBloc = ".gestion_" + $this.data('emplacement');
            const filenameComponent =  chemin_plugin + `z-dsfr/design_system_admin/formulaires/composants/composant_generateur_${menuType}_${nomElements}.php`;    
            var files = [];
            // Parcours du nombre de sous-menus déroulants
            for(let i = 1; i <= nombreBlocMega; i++){
                const nouvelElement = doitAjouterNouveauxLiens(`menu-megamenu-titre-menu-bloc${i}`); 
                if(nouvelElement){
                    var dataLink = {
                        autreparametre: i,
                        chemin_plugin: chemin_plugin
                    };                
                    files.push({ filename: filenameComponent, data: dataLink, emplacement: emplacementBloc });
                    // Si un nouvel élément doit être ajouté, construit le champ de formulaire et l'ajoute
                    //construireLesChampsDuFormulaire($(this), emplacementAjout, '',  fileNameElement, i);
                }
            }
            processFilesSequentially(files);
            const selectCourant = $this.attr('name');
            const nombreOptionsSelect = $(`[name="${selectCourant}"] option`).length;        
            for(let j = parseInt(nombreBlocMega) + 1; j <= nombreOptionsSelect - 1; j++) {
                $(`.colonne${j}`).remove();
            }
        });
    
        $(document).on('change', '[data-commonname="megamenu_bloc"]', function() {
            const $this = $(this);
            const nombreBlocMega = $this.val();
            const menuType = $this.data('menutype');
            //const nomElements = $this.data('emplacement');
            const emplacementBloc = $this.data('emplacement');
            const emplacementGenerateurs = `.${emplacementBloc}`;
            const filenameComponent =  chemin_plugin + `z-dsfr/design_system_admin/formulaires/composants/composant_generateur_${menuType}_block_menu.php`;
            const menuNumber = $this.data('menunumber');
            var files = [];
            for(let i = 1; i <= nombreBlocMega; i++) {
                const nouvelElement = doitAjouterNouveauxLiens(`titre${i}menu${menuNumber}`); 
                if(nouvelElement){
                    const mesdatas = {
                        autreparametre: i,
                        menuNumber: menuNumber,
                        chemin_plugin: chemin_plugin
                    };
                    
                    files.push({ filename: filenameComponent, data: mesdatas, emplacement: emplacementGenerateurs });
                }
            }
            processFilesSequentially(files);
            const selectCourant = $this.attr('name');
            const nombreOptionsSelect = $(`[name="${selectCourant}"] option`).length;
        
            for(let j = parseInt(nombreBlocMega) + 1; j <= nombreOptionsSelect; j++) {
                const aEffacer = `.menu_${menuNumber}_${j}`;
                $(aEffacer).remove();
            }
        });
    
        $(document).on('click', 'button.autoriser_reinit_form', function(e) { 
            e.preventDefault;
            $('[name="submit_generateurs_menu"]').attr("value", "Générer l'élément");
            $('button.autoriser_reinit_form').addClass('dontshow');        
            $('#resultats').remove();
            $('.les-resultats fieldset').html('');

            return false;
        });

        function createMenuDeroulantResult(nombreDeSousMenus) {
            var resultat = 'deroulant;' + $('[name="menu_deroulant_titre"]').val() + ';' + nombreDeSousMenus + ';' + $('[name="menu_deroulant_icon"]').val() + ';';
            for(let i = 1; i <= nombreDeSousMenus; i++){
                resultat += $('[name="menuderoulant_titre_' + i + '"]').val() + ';' + $('[name="menuderoulant_url_' + i + '"]').val();
                if(i < nombreDeSousMenus){
                    resultat += ';';
                }
            }
            resultat += ';';
            return resultat;
        }

    
        $('form#menu_generator').on('submit', function(e) {
            e.preventDefault();
        
            var uniqueRandomVar = Math.random().toString(36).substr(2) + Date.now().toString(36);
            const submitButton = $('[name="submit_generateurs_menu"]');
            
            if (!this.checkValidity()) {
                return;
            }
        
            var typeMenuType = $('[name="type-menu-type"]').val();
            var elementParent = $('#modal').attr('data-parent');
            var emplacement = $('.les-resultats fieldset');
            var filename = 'z-dsfr/design_system_admin/formulaires/composants/composant_generateur_' + typeMenuType + '_resultat.php?' + uniqueRandomVar;
            $('button.autoriser_reinit_form').removeClass('dontshow');
            $('.les-resultats fieldset').removeClass('dontshow');
            switch (typeMenuType) {
                case 'menu_simple':
                    processMenuSimple(typeMenuType, submitButton, elementParent, emplacement, filename);
                    break;
                case 'menu_deroulant':
                    processMenuDeroulant(typeMenuType, submitButton, elementParent, emplacement, filename);
                    break;
                case 'megamenu':
                    processMegaMenu(typeMenuType, submitButton, elementParent, emplacement, filename);
                    break;
                default:
                break;
            }
        });
    
        $(document).on('click', '.menu_deroulant_btn', function(e) { 
            e.preventDefault();
            var $this = $(this); // Réduire les appels à $(this)
            var ariaExpanded = $this.attr('aria-expanded') === 'true'; // Convertir en booléen pour simplifier la comparaison
            var $menu = $('#menu-646d00ee27a64'); // Stocker la référence au menu pour éviter de répéter le sélecteur
            $this.attr('aria-expanded', !ariaExpanded); // Inverser la valeur de aria-expanded
            $menu.css('display', ariaExpanded ? 'none' : 'block'); // Utiliser l'opérateur ternaire pour simplifier l'instruction if/else
        });

        $(document).on('click', 'button.autoriser_chargement_nouveau_second_logo', function(e) { 
            e.preventDefault();
            const filename =  chemin_plugin + `z-dsfr/design_system_admin/formulaires/composants/composant_masquer_logo_secondaire.php`;
            const data = {chemin_plugin: chemin_plugin};
            const emplacement = $('.gestion_masquer_logo_secondaire');
            $('.suppression_image').remove();
            createAjaxRequest(filename, data, emplacement) ;
        });

        $(document).on('click', '.l8020 button', function(e) { 
            e.preventDefault();
        });

        $('#modal .close').on('click', function(eevent) {
            eevent.preventDefault();
            $('.affichage_generateur').remove();
            $('[name="submit_generateurs_menu"]').addClass('dontshow');
            $('[name="type-menu-type"]').val('');
            $('.les-resultats').addClass('dontshow');
            $('#modal').addClass('dontshow');
        });
    }
    $('button.questionmark_header').on('click', function(eevent) {
        eevent.preventDefault();
        $this = $(this);
        const buttonValue = $this.find('span').text();
        const parent = $this.data('parent');
        const fille =  $this.data('fille');
        const filename = chemin_plugin + 'z-dsfr/design_system_admin/formulaires/aide/' + fille + '.php';
        const datalink = {chemin_plugin: chemin_plugin};
        if (buttonValue === 'X'){
            $(`.${fille}`).remove();
            $this.find('span').text('?');
            $(this).removeAttr('title').attr('title','Afficher la fenêtre d\'aide');
        } else {
            $this.find('span').text('X');
            createAjaxRequest(filename, datalink, `.${parent}`);
            $(this).removeAttr('title').attr('title','Fermer la fenêtre d\'aide');
        }
    });
    
    $('button.questionmark').on('click', function(eevent) {
        eevent.preventDefault();
        $this = $(this);
        const buttonValue = $this.text();
        const parentForm = $this.data('parent');
        const formComponents = `${parentForm}_primaire`;
        const formHelp = `${parentForm}_secondaire`;
        const emplacement = `aide_${parentForm}`;
        const filename =  chemin_plugin + `z-dsfr/design_system_admin/formulaires/aide/${emplacement}.php`;
        const datalink = {chemin_plugin: chemin_plugin};
        if (buttonValue === 'X'){
            $(`.${formHelp}`).remove();
            $this.text('?');
            $(`.${emplacement}`).removeClass('overlay');
            $(`.${formComponents}`).removeClass('dontshow');
        } else{
            $(`.${emplacement}`).addClass('overlay');
            $(`.${formComponents}`).addClass('dontshow');
            $this.text('X');
            createAjaxRequest(filename, datalink, `.${emplacement}`);
        }
    });

    // Fonction pour valider et soumettre le formulaire
    function validateAndSubmitForm() {
        var form = $('#dsfr_configuration');

        // Supprimer la classe 'validitynone' de tous les champs
        form.find('.validitynone').removeClass('validitynone');

        if (form[0].checkValidity()) { // Vérifie si tous les champs du formulaire sont valides
            form[0].submit(); // Utiliser la méthode DOM native pour soumettre le formulaire
        } else {
            // Ajouter un message d'erreur ou des actions à effectuer en cas de non-validité.
            console.error("Des champs obligatoires ne sont pas remplis correctement.");

            // Trouver tous les champs invalides, leur donner le focus et leur ajouter la classe 'validitynone'
            var firstInvalid = null;
            form.find(':input').each(function() {
                if (!this.validity.valid) {
                    $(this).addClass('validitynone');
                    if (!firstInvalid) {
                        firstInvalid = this;
                    }
                }
            });

            // Focaliser le premier champ invalide
            if (firstInvalid) {
                firstInvalid.focus();
                // Faire défiler jusqu'au premier champ invalide si nécessaire
                var offset = firstInvalid.offset().top;
                $('html, body').animate({
                    scrollTop: offset
                }, 500);
            }
        }
    }

    // Gestionnaire d'événement pour Ctrl + Enter
    $(document).on('keydown', function(e) {
        if (e.keyCode === 13 && e.ctrlKey) {
            e.preventDefault();
            validateAndSubmitForm();
        }
    });

    // Gestionnaire d'événement pour la soumission du formulaire
    $('.dsa_style_sticky input[type="submit"]').on('click', function(e) {
        e.preventDefault();
        // Appeler votre fonction ici
        validateAndSubmitForm();
    });
    
    // Top button
    var btn = $('#topbtn');            
    $(window).scroll(function() {
        if ($(window).scrollTop() > 100) {
        btn.addClass('show');
        } else {
        btn.removeClass('show');
        }
    });            
    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '100');
    });   
    
    

});