<?php

/**
 * Utilisations de pipelines par Design system Admin
 *
 * @plugin     Design system Admin
 * @copyright  2023
 * @author     Mikaël Folio
 * @licence    GNU/GPL
 * @package    SPIP\Design_system_admin\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/*
 * Un fichier de pipelines permet de regrouper
 * les fonctions de branchement de votre plugin
 * sur des pipelines existants.
 */
