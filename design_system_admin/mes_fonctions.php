<?php

date_default_timezone_set("Europe/Paris");
define(
    '_ID_WEBMESTRES',
	'1:2');

	
// sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

define('_MAX_MOTS_LISTE', 100);

//define('_DUREE_CACHE_DEFAUT', 3600);

define('_MAX_NOMBRE_DE_LIENS_LDL', 6);
define('_MAX_NOMBRE_DE_LIENS_MENUS', 9);
define('_MAX_NOMBRE_DE_LIENS_PARTENAIRES_PRINCIPAUX', 2);
define('_MAX_NOMBRE_DE_LIENS_PARTENAIRES_SECONDAIRES', 4);
define('_MAX_NOMBRE_DE_LIENS_ECOSYSTEME', 4);
define('_MAX_NOMBRE_DE_LIENS_LEGAUX', 8);




// jQuery Async loader / Defer parsing of JavaScript
// cf https://gist.github.com/tech-nova/85ce2dd05e3596571369 et http://www.yterium.net/jQl-an-asynchronous-jQuery-Loader
define('_JS_ASYNC_LOAD',true);

// Protection Spam formulaire
	$GLOBALS['formulaires_no_spam'][] = 'nous_contacter';
	$GLOBALS['formulaires_no_spam'][] = 'votre_avis';
	$GLOBALS['formulaires_no_spam'][] = 'question_posee';

    function lnkclass($texte) {
		// inclusion des CLASS perso Part One : début de la CLASS
		$texte = str_replace('#+','" class="',$texte);
		// inclusion des CLASS perso Part Two : fin de la CLASS
					$texte = str_replace('+#','',$texte);
					$texte = str_replace('+',' ',$texte);

		
		return $texte;
	}
//SPIP offre une option pour souligner la requête au sein des pages
//define('_SURLIGNE_RECHERCHE_REFERERS',true);
// Définition des administrateurs du site
//if (isset($_REQUEST['recherche'])) {
//  $_GET['var_recherche'] = $_REQUEST['recherche'];
//}
/**
 * Une fonction récursive pour joliment afficher #ENV, #GET, #SESSION...
 *    en squelette : [(#ENV|bel_env)], [(#GET|bel_env)], [(#SESSION|bel_env)]
 *    ou encore [(#ARRAY{0,1, a,#SESSION, 1,#ARRAY{x,y}}|bel_env)]
 *
 * @param string|array $env
 *    si une string est passée elle doit être le serialize d'un array 
 *
 * @return string
 *    une chaîne html affichant une <table>
**/
function bel_env($env) {
    $env = str_replace(array('&quot;', '&#039;'), array('"', '\''), $env);
    if (is_array($env_tab = @unserialize($env))) {
        $env = $env_tab;
    }
    if (!is_array($env)) {
        return '';
    }
    $style = " style='border:1px solid #ddd;'";
    $res = "<table style='border-collapse:collapse;'>\n";
    foreach ($env as $nom => $val) {
        if (is_array($val) || is_array(@unserialize($val))) {
            $val = bel_env($val);
        }
        else {
            $val = entites_html($val);
        }
        $res .= "<tr>\n<td$style><strong>". entites_html($nom).
                   "&nbsp;:&nbsp;</strong></td><td$style>" .$val. "</td>\n</tr>\n";
    }
    $res .= "</table>";
    return $res;
}

function noP($texte){
    $texte = preg_replace("@</p>@iS", "", $texte);
    $texte = preg_replace("@<p\b.*>@UiS", "", $texte);
    return $texte;
}

function datetoen($texte){
    $date_fr = $texte;
    // Convertir la date en timestamp
    $timestamp = strtotime($date_fr);
    // Reformater la date en anglais
    $date_en = date("F j, Y", $timestamp);
    // Afficher la date en anglais
    return $date_en; // Output: "February 17, 1972"
}
function balise_CONFIG_VALUE_dist($p) {
    // Récupérer le premier argument passé à la balise (il doit s'agir du nom de l'option)
    $option_name = interprete_argument_balise(1,$p);

    // Assurez-vous qu'un nom d'option a été passé
    if (!$option_name) {
        $p->code = "''";
        $p->statut = 'php';
        return $p;
    }

    // Utilisez sql_quote pour sécuriser la valeur de l'option
    $option_name = 'sql_quote(' . $option_name . ')';

    // Effectuer une requête pour récupérer la valeur de l'option
    $p->code = 'sql_getfetsel(\'option_value\', \'spip_dsfrconfigurations\', \'option_name=\' . ' . $option_name . ')';
    $p->statut = 'php';
    
    return $p;
}

function filtre_constante($constante) {
    // design_admin_system_options.php
    $const_name = '_' . strtoupper($constante);
    if (defined($const_name)) {
        return constant($const_name);
    } else {
        return '99';
    }
}

// Gestion activation / désactivation des différents options de l page de configuration
function balise_VA_ET_VIENT_dist($p) {
    $id_checkbox = interprete_argument_balise(1,$p);
    $getoption = interprete_argument_balise(2,$p);

    $p->code = 'calculer_balise_VA_ET_VIENT(' . $id_checkbox . ', ' . $getoption . ')';
    $p->statut = 'php';

    return $p;
}
function calculer_balise_VA_ET_VIENT($id_checkbox, $getoption, $activation = '') {
    
    $html = '
	<div class="ic-Super-toggle--on-off">
	  	<input type="checkbox" id="'.$id_checkbox.'" class="ic-Super-toggle__input"'. (($getoption === 'oui') ? ' checked' : '' ).' value="'.$getoption.'" name="'.$id_checkbox.'" data-information="commandes" data-link="'.find_in_path('design_system_admin').'">
	  	<label class="ic-Super-toggle__label" for="'.$id_checkbox.'">
			<div class="ic-Super-toggle__screenreader">Enable flux capacitor</div>
			<div class="ic-Super-toggle__disabled-msg" data-checked="On" data-unchecked="Bouton désactivé" aria-hidden="true"></div>
			<div class="ic-Super-toggle-switch" aria-hidden="true">
				<div class="ic-Super-toggle-option-LEFT" aria-hidden="true">
					<svg class="ic-Super-toggle__svg" xmlns="http://www.w3.org/2000/svg" version="1.1" x="0" y="0" width="548.9" height="548.9" viewBox="0 0 548.9 548.9" xml:space="preserve">
					<polygon points="449.3 48 195.5 301.8 99.5 205.9 0 305.4 95.9 401.4 195.5 500.9 295 401.4 548.9 147.5 " fill="#fff" />
					</svg>
				</div>
				<div class="ic-Super-toggle-option-RIGHT" aria-hidden="true">
					<svg class="ic-Super-toggle__svg" xmlns="http://www.w3.org/2000/svg" version="1.1" x="0" y="0" width="28" height="28" viewBox="0 0 28 28" xml:space="preserve">
					<polygon points="28 22.4 19.6 14 28 5.6 22.4 0 14 8.4 5.6 0 0 5.6 8.4 14 0 22.4 5.6 28 14 19.6 22.4 28 " fill="#fff" />
					</svg>
				</div>
			</div>
	  	</label>
	</div>
	<input type="hidden" name="_'.$id_checkbox.'" value="'.$getoption.'" />
	';

    return $html;
}
// récupérer la valeur d'une constante pour pouvoir l'utiliser en javascript
function balise_TABLEAU_PLAGE_dist($p) {
    $nom_constante = interprete_argument_balise(1,$p);
    if (!$nom_constante) {
        $p->error = 'La balise #TABLEAU_PLAGE doit être utilisée avec un nombre.';
        return $p;
    }
    $p->code = 'generer_tableau_plage(' . str_replace('\'', '"', $nom_constante) . ')';
    $p->interdire_scripts = false;
    return $p;
}

function generer_tableau_plage($nom_constante) {
    $nombre = constant($nom_constante);
    $result = array();
    for ($i = 1; $i <= $nombre; $i++) {
        $result[$i] = $i;
    }
    return $result;
}


// Récupération de la valeur d'une variable de session pour pouvoir l'utiliser en javascript ou en SPip (balise dynamique). Appel par #VARIABLE_SESSION{nom_variable}
function balise_VARIABLE_SESSION_dist($p) {
    // Récupérer le premier argument passé à la balise (il doit s'agir du nom de la variable de session)
    $nom_variable = interprete_argument_balise(1, $p);

    // Assurez-vous qu'un nom de variable de session a été passé
    if (!$nom_variable) {
        $p->code = "''";
        $p->statut = 'php';
        return $p;
    }

    // Utilisez la variable de session avec le nom spécifié
    $p->code = '$_SESSION[' . $nom_variable . ']';
    $p->statut = 'php';


    return $p;
}

/**
 * Retourne le nom de domaine d'une url
 *
 */
function getNomDeDomaine() {
    return $_SERVER['HTTP_HOST'];
}
// Déclaration du filtre personnalisé
function filtre_tableau_element($texte, $delimiteur=';') {
    $valeurs = explode($delimiteur, $texte);
    return '<li><a class="fr-btn fr-btn--display '. $valeurs[3] .'" href="' . $valeurs[2] . '">' . $valeurs[1] . '</a></li>';
}

function getDomainFrom($url)
  {
    $pieces = parse_url($url);
    $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
    return $regs['domain'];
    }
    return false;
  }

// Déclaration menu simple
function filtre_menu($texte, $delimiteur=';') {
    $prefix = php_uname('n'); 
    $uniqueId = uniqid($prefix, true);
    $valeurs = explode($delimiteur, $texte);
    $menu_cree = '';
    $menu_en_cours = 0;
    $aria_current = '';
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    $url_server = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]".parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
    if (basename($url_server) == 'index.php') {
        $url_server = dirname($url);
    }
    switch($valeurs[0]){
        case "direct":            
            $url = $valeurs[2];
            if(strpos($url, '/') === 0) {
                $url = $protocol.$domainName.$url; 
            }            
            $aria_current = ($url === $url_server) ? 'aria-current="page"' : '';
            $menu_cree = '<li class="fr-nav__item '. $valeurs[3] .'" data-fr-js-navigation-item="true"><a class="fr-nav__link" href="' . $valeurs[2] . '" target="_self" '.$aria_current.'>' . $valeurs[1] . '</a></li>';
        break;
        case "deroulant":
            $menu_cree = '
            <li class="fr-nav__item" data-fr-js-navigation-item="true">
                <button class="fr-nav__btn" aria-expanded="false" aria-controls="menu-'.$uniqueId.'" data-fr-js-collapse-button="true">'.$valeurs[1].'</button>
                <div class="fr-collapse fr-menu" id="menu-'.$uniqueId.'" data-fr-js-collapse="true">
                    <ul class="fr-menu__list">';
            
                        $menu_en_cours = 4;
                        if (count($valeurs) > $menu_en_cours) {
                            for ($i = $menu_en_cours; ($i + 2) < count($valeurs); $i += 2) {
                                $texte = $valeurs[$i];
                                $url = $valeurs[$i + 1];
                                if(strpos($url, '/') === 0) {
                                    $url = $protocol.$domainName.$url; 
                                }

                                $aria_current = ($url === $url_server) ? 'aria-current="page"' : '';
                                $menu_cree .= '<li><a class="fr-nav__link" href="' . $url . '" target="_self" ' . $aria_current . '>' . $texte . '</a></li>';
                            }
                        }

                    $menu_cree .= '</ul>
                </div>
            </li>';
        break;
        case "megamenu":
            

           
            
            #megamenu;3;Climat;/plugins-dist/z-dsfr/design_system_child/prive/themes/spip/images/theme-1-navigation.svg;Climat;div;L’augmentation dans l’atmosphère de la concentration en gaz à effet de serre (GES) résultant des activités humaines (notamment la combustion des énergies fossiles, l’utilisation d’engrais de synthèse et la production de GES artificiels tels que les gaz réfrigérants ) perturbe les équilibres climatiques de long terme à l’échelle planétaire.L’ampleur du réchauffement et ses effets se concrétisent de façon différente (température, régime des pluies, montée des eaux, fréquence et ampleur des phénomènes météorologiques extrêmes, etc.) selon les régions et leur vulnérabilité.;Voir le thème;/theme/climat;menu1;;Comprendre le changement climatique;/themes/climat/article/comprendre-le-changement-climatique;L’adaptation au changement climatique;/themes/climat/article/l-adaptation-au-changement-climatique;menu2;;Les émissions de gaz à effet de serre et l’empreinte carbone;/themes/climat/article/les-emissions-de-gaz-a-effet-de-serre-et-l-empreinte-carbone;menu3;;Limitation des émissions de gaz à effet de serre;/themes/climat/article/limitation-des-emissions-de-gaz-a-effet-de-serre;
            $menu_cree = '
            <li class="fr-nav__item" data-fr-js-navigation-item="true">
                <button class="fr-nav__btn" aria-expanded="false" aria-controls="mega-menu-'.$uniqueId.'" data-fr-js-collapse-button="true">'.$valeurs[2].'</button>
                <div class="fr-collapse fr-mega-menu fr-collapse--expanded" id="mega-menu-'.$uniqueId.'" data-fr-js-collapse="true">
                    <div class="fr-container fr-container--fluid fr-container-lg">
                        <div class="fr-grid-row fr-grid-row-lg--gutters">
                            <div class="fr-col-12 fr-mb-n3v">
                            <button class="fr-link--close fr-link" aria-controls="mega-menu-'.$uniqueId.'" data-fr-js-collapse-button="true">Fermer</button>
                            </div>
                            <div class="fr-col-12  d-flex fr-grid-row fr-grid-row-lg--gutters">';
                            if (strlen($valeurs[3]) > 5) {
                                $menu_cree .= '<div class="dropdown-menu__image fr-col-2 ">
                                    <img loading="lazy" class="arrondi" src="'.$valeurs[3].'" alt="" width="200">
                                </div>';
                            }
                            $menu_cree .= '<div class="fr-mega-menu__leader fr-pl-4w fr-col-12 fr-col-md-10">
                                    <'.$valeurs[5].' class="fr-h4 fr-mb-2v">'.$valeurs[2].'</'.$valeurs[5].'>
                                    <div class="fr-my-3v">'.$valeurs[6].'</div>
                                    <a class="fr-link fr-fi-arrow-right-line fr-link--icon-right" href="'.$valeurs[8].'">'.$valeurs[7].'</a>';
                                    $temp_texte =  trim($texte, ";");
                                    if ($valeurs[1] > 0){
                                        $menu_cree .= '<div class="fr-grid-row fr-grid-row-lg--gutters fr-mt-4v">'; 
                                        if ($valeurs[1] > 2) {                                            
                                            // Trouver l'indice de "menu3"
                                            $indexMenu3 = array_search('menu3', $valeurs);
                                            
                                            // Extraire les valeurs à partir de "menu3"
                                            $menu3 = [];
                                            $menu3 = array_slice($valeurs, $indexMenu3 + 1);
                                            array_pop($menu3);
                                            
                                        }  
                                        if ($valeurs[1] > 1) {
                                             // Trouver les indices de "menu1" et "menu2"
                                             $indexMenu2 = array_search('menu2', $valeurs);
                                             $indexMenu3 = array_search('menu3', $valeurs);
 
                                             // Extraire les valeurs entre "menu2" et "menu3"
                                             $menu2 = [];
                                             for ($i = $indexMenu2 + 1; $i < $indexMenu3; $i++) {
                                                 $menu2[] = $valeurs[$i];
                                             }
                                        }                                      
                                        if ($valeurs[1] > 0) {
                                            // Trouver les indices de "menu1" et "menu2"
                                            $indexMenu1 = array_search('menu1', $valeurs);
                                            $indexMenu2 = array_search('menu2', $valeurs);

                                            // Extraire les valeurs entre "menu2" et "menu3"
                                            $menu1 = [];
                                            for ($i = $indexMenu1 + 1; $i < $indexMenu2; $i++) {
                                                $menu1[] = $valeurs[$i];
                                            }

                                            
                                        }                                      
                                        for($n = 1; $n <= $valeurs[1]; $n++){
                                            $menu_n = 'menu'.$n;
                                            $menu_cree = $menu_cree . '<div class="fr-col-12 fr-col-lg-4 ">
                                                <div class="fr-mega-menu__category ">
                                                    <p class="fr-nav__link">'.$$menu_n[0].'</p>
                                                </div>
                                                <ul class="fr-mega-menu__list">';
                                                    // Extraire les données du tableau par groupe de deux, sauf le premier élément
                                                    $resultat = [];
                                                    $longueur = count($$menu_n);
                                                    for ($i = 1; $i < $longueur; $i += 2) {
                                                        $menu_cree .= '<li><a class="fr-nav__link" href="'.$$menu_n[$i+1].'" target="_self">'.$$menu_n[$i].'</a></li>';
                                                    }
                                                $menu_cree .= '
                                                </ul></div>';
                                        }                                        
                                    }
                                    $menu_cree .= '
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>'; 
        break;
        default:
        break;
    }

    return $menu_cree;
}
?>