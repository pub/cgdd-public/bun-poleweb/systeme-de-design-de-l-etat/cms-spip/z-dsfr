<?php

/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Design system Admin
 * @copyright  2023
 * @author     Mikaël Folio
 * @licence    GNU/GPL
 * @package    SPIP\Design_system_admin\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function design_system_admin_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['dsfrconfigurations'] = 'dsfrconfigurations';

	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */

 function inserer_jquery($flux) {
    // Vérifiez si la page actuelle est la page de configuration de votre plugin
    if (_request('exec') == 'configurer_design_system_admin') {
        $jquery_script = '<script src="/plugins-dist/z-dsfr/design_system_admin/prive/javascript/jquery.min.js"></script>';
        $flux .= $jquery_script;
    }
    return $flux;
}

function design_system_admin_declarer_tables_objets_sql($tables) {
	include_spip('base/abstract_sql');

	// Déclaration de la table spip_dsfrconfigurations
	$tables['spip_dsfrconfigurations'] = array(
		'type' => 'dsfrconfiguration',
		'principale' => 'oui',
		'field' => array(
			'id_dsfrconfiguration' => 'bigint(21) NOT NULL',
			'option_name' => 'text NOT NULL',
			'option_value' => 'text NOT NULL',
			'maj' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
		),
		'key' => array(
			'PRIMARY KEY' => 'id_dsfrconfiguration',
		),
		'titre' => 'option_name AS titre, "" AS lang',
		'champs_editables' => array(),
		'champs_versionnes' => array(),
		'rechercher_champs' => array(),
		'tables_jointures' => array(),
	);

	// Vérification des enregistrements existants
	$enregistrements_existant = sql_countsel('spip_dsfrconfigurations');
	if ($enregistrements_existant == 0) {
		// Insertion des valeurs par défaut dans la table spip_dsfrconfigurations
		$insertion_defaut = array(
			array(
				'option_name' => 'nom_de_domaine_en_production',
				'option_value' => 'localhost',
			),
			array(
				'option_name' => 'title_onglet_navigateur',
				'option_value' => 'Wordpress Thème DSFR',
			),
			array(
				'option_name' => 'meta_description',
				'option_value' => 'Contrairement à une opinion répandue, le Lorem Ipsum n\'est pas simplement du texte aléatoire. Il trouve ses racines dans une oeuvre de la littérature latine classique datant de 45 av. J.-C.',
			),
			array(
				'option_name' => 'address_email_responsable',
				'option_value' => 'mikael.folio@developpement-durable.gouv.fr',
			),
			array(
				'option_name' => 'form_contact',
				'option_value' => '/contact',
			),
			array(
				'option_name' => 'texte_marianne',
				'option_value' => 'Gouvernement',
			),
			array(
				'option_name' => 'liens_externe_que_faire',
				'option_value' => 'automatique',
			),
			array(
				'option_name' => '_masquer_le_titre',
				'option_value' => 'non',
			),
			array(
				'option_name' => 'titre_du_site',
				'option_value' => 'Thème DSFR pour SPIP',
			),
			array(
				'option_name' => '_masquer_le_slogan',
				'option_value' => 'non',
			),
			array(
				'option_name' => 'slogan_du_site',
				'option_value' => 'Thème 2.0 DSFR 1.8.5 pour SPIP 4.2',
			),
			array(
				'option_name' => '_drapeau_en_berne',
				'option_value' => 'non',
			),
			array(
				'option_name' => '_masquer_haut_de_page',
				'option_value' => 'non',
			),
			array(
				'option_name' => '_home_en_h1',
				'option_value' => 'non',
			),
			array(
				'option_name' => '_masquer_auteur',
				'option_value' => 'oui',
			),
			array(
				'option_name' => 'contact_auteur',
				'option_value' => 'oui',
			),
			array(
				'option_name' => '_masquer_contact',
				'option_value' => 'oui',
			),
			array(
				'option_name' => 'titre_lien_contact',
				'option_value' => 'Nous contacter',
			),
			array(
				'option_name' => 'id_auteur_contact',
				'option_value' => '1',
			),
			array(
				'option_name' => 'emplacement_contact',
				'option_value' => '7',
			),
			array(
				'option_name' => '_masquer_recherche',
				'option_value' => 'non',
			),
			array(
				'option_name' => '_masquer_message_alerte',
				'option_value' => 'non',
			),
			array(
				'option_name' => 'message_alerte',
				'option_value' => 'Ceci est la première installation du plugin DSFR pour spip. <a href="/ecrire/?exec=configurer_design_system_admin">Cliquer ici</a> pour accéder à la page de configuration.',
			),
			array(
				'option_name' => '_masquer_statistiques',
				'option_value' => 'oui',
			),
			array(
				'option_name' => 'script_statistiques',
				'option_value' => '',
			),
			array(
				'option_name' => '_masquer_liste_de_lien',
				'option_value' => 'oui',
			),
			array(
				'option_name' => 'type_de_liste_de_liens',
				'option_value' => 'interne',
			),
			array(
				'option_name' => 'nombre_de_liste_de_liens',
				'option_value' => '1',
			),
			array(
				'option_name' => 'liste_de_liens_1',
				'option_value' => '',
			),
			array(
				'option_name' => 'liste_de_liens_2',
				'option_value' => '',
			),
			array(
				'option_name' => 'liste_de_liens_3',
				'option_value' => '',
			),
			array(
				'option_name' => 'liste_de_liens_4',
				'option_value' => '',
			),
			array(
				'option_name' => 'liste_de_liens_5',
				'option_value' => '',
			),
			array(
				'option_name' => 'liste_de_liens_6',
				'option_value' => '',
			),
			array(
				'option_name' => '_masquer_menu',
				'option_value' => 'non',
			),
			array(
				'option_name' => 'type_de_menu',
				'option_value' => 'Interne',
			),
			array(
				'option_name' => 'nombre_de_menu',
				'option_value' => '1',
			),
			array(
				'option_name' => 'menu_1',
				'option_value' => 'direct;Accueil;/;',
			),
			array(
				'option_name' => 'menu_2',
				'option_value' => '',
			),
			array(
				'option_name' => 'menu_3',
				'option_value' => '',
			),
			array(
				'option_name' => 'menu_4',
				'option_value' => '',
			),
			array(
				'option_name' => 'menu_5',
				'option_value' => '',
			),
			array(
				'option_name' => 'menu_6',
				'option_value' => '',
			),
			array(
				'option_name' => 'menu_7',
				'option_value' => '',
			),
			array(
				'option_name' => 'menu_8',
				'option_value' => '',
			),
			array(
				'option_name' => 'menu_9',
				'option_value' => '',
			),
			array(
				'option_name' => '_masquer_partenaires',
				'option_value' => 'oui',
			),
			array(
				'option_name' => 'titre_du_bloc_partenaires',
				'option_value' => '',
			),
			array(
				'option_name' => 'nombre_de_partenaires_principaux',
				'option_value' => '1',
			),
			array(
				'option_name' => 'partenaires_principaux_1',
				'option_value' => '',
			),
			array(
				'option_name' => 'partenaires_principaux_2',
				'option_value' => '',
			),
			array(
				'option_name' => 'nombre_de_partenaires_secondaires',
				'option_value' => '2',
			),
			array(
				'option_name' => 'partenaires_secondaires_1',
				'option_value' => '',
			),
			array(
				'option_name' => 'partenaires_secondaires_2',
				'option_value' => '',
			),
			array(
				'option_name' => 'partenaires_secondaires_3',
				'option_value' => '',
			),
			array(
				'option_name' => 'partenaires_secondaires_4',
				'option_value' => '',
			),
			array(
				'option_name' => '_masquer_cookies',
				'option_value' => 'oui',
			),
			array(
				'option_name' => 'liste_des_cookies',
				'option_value' => '',
			),
			array(
				'option_name' => 'modifier_texte_cookies',
				'option_value' => 'non',
			),
			array(
				'option_name' => 'titre_texte_cookies',
				'option_value' => '',
			),
			array(
				'option_name' => 'texte_cookies',
				'option_value' => '',
			),
			array(
				'option_name' => '_masquer_parametres_affichage',
				'option_value' => 'non',
			),
			array(
				'option_name' => 'parametre_affichage_defaut',
				'option_value' => 'dark',
			),
			array(
				'option_name' => 'pad_dans_footer',
				'option_value' => 'oui',
			),
			array(
				'option_name' => '_masquer_liste_acces_rapide',
				'option_value' => 'non',
			),
			array(
				'option_name' => 'lien_dacces_rapide1',
				'option_value' => '',
			),
			array(
				'option_name' => 'lien_dacces_rapide2',
				'option_value' => '',
			),
			array(
				'option_name' => 'lien_dacces_rapide3',
				'option_value' => '',
			),
			array(
				'option_name' => '_masquer_la_newsletter',
				'option_value' => 'oui',
			),
			array(
				'option_name' => 'titre_form_newsletter',
				'option_value' => '',
			),
			array(
				'option_name' => 'slogan_form_newsletter',
				'option_value' => '',
			),
			array(
				'option_name' => 'description_newsletter',
				'option_value' => '',
			),
			array(
				'option_name' => 'type_formulaire_newsletter',
				'option_value' => 'formulaire',
			),
			array(
				'option_name' => 'url_formulaire_newsletter',
				'option_value' => '',
			),
			array(
				'option_name' => 'titre_bouton_sabonner',
				'option_value' => 'S\'abonner',
			),
			array(
				'option_name' => 'nom_champ_input_form',
				'option_value' => '',
			),
			array(
				'option_name' => '_masquer_partage_reseaux_sociaux',
				'option_value' => 'oui',
			),
			array(
				'option_name' => 'titre_partage_reseaux_sociaux',
				'option_value' => '',
			),
			array(
				'option_name' => 'url_facebook',
				'option_value' => '',
			),
			array(
				'option_name' => 'url_twitter',
				'option_value' => '',
			),
			array(
				'option_name' => 'url_youtube',
				'option_value' => '',
			),
			array(
				'option_name' => 'url_instagram',
				'option_value' => '',
			),
			array(
				'option_name' => 'url_linkedin',
				'option_value' => '',
			),
			array(
				'option_name' => 'url_rss',
				'option_value' => '',
			),
			array(
				'option_name' => '_masquer_logo_secondaire',
				'option_value' => 'oui',
			),
			array(
				'option_name' => 'type_insertion_second_logo',
				'option_value' => '',
			),
			array(
				'option_name' => 'second_logo_upload',
				'option_value' => '',
			),
			array(
				'option_name' => 'nombre_de_liens_ecosysteme',
				'option_value' => '4',
			),
			array(
				'option_name' => 'lien_eco_1',
				'option_value' => 'direct;data.gouv.fr;https://data.gouv.fr/;',
			),
			array(
				'option_name' => 'lien_eco_2',
				'option_value' => 'direct;service-public.fr;https://service-public.fr/;',
			),
			array(
				'option_name' => 'lien_eco_3',
				'option_value' => 'direct;gouvernement.fr;https://gouvernement.fr/;',
			),
			array(
				'option_name' => 'lien_eco_4',
				'option_value' => 'direct;legifrance.gouv.fr;https://legifrance.gouv.fr/;',
			),
			array(
				'option_name' => 'lien_eco_5',
				'option_value' => '',
			),
			array(
				'option_name' => 'lien_eco_6',
				'option_value' => '',
			),
			array(
				'option_name' => 'lien_eco_7',
				'option_value' => '',
			),
			array(
				'option_name' => 'lien_eco_8',
				'option_value' => '',
			),
			array(
				'option_name' => 'description_footer',
				'option_value' => '',
			),
			array(
				'option_name' => '_masquer_liens_obligation_legale',
				'option_value' => 'non',
			),
			array(
				'option_name' => 'nombre_de_liens_obligations_legales',
				'option_value' => '3',
			),
			array(
				'option_name' => 'lien_legal_1',
				'option_value' => 'direct;accessibilité : non/partiellement/totalement conforme;/accessibilite/;',
			),
			array(
				'option_name' => 'lien_legal_2',
				'option_value' => 'direct;Données personnelles;/donnees-personnelles/;',
			),
			array(
				'option_name' => 'lien_legal_3',
				'option_value' => 'direct;Mentions légales;/mentions-legales/;',
			),
			array(
				'option_name' => 'lien_legal_4',
				'option_value' => '',
			),
			array(
				'option_name' => 'lien_legal_5',
				'option_value' => '',
			),
			array(
				'option_name' => 'lien_legal_6',
				'option_value' => '',
			),
			array(
				'option_name' => 'lien_legal_7',
				'option_value' => '',
			),
			array(
				'option_name' => 'lien_legal_8',
				'option_value' => '',
			),
			array(
				'option_name' => 'afficher_bouton_cookies',
				'option_value' => 'oui',
			),
			array(
				'option_name' => 'dsfr_licence',
				'option_value' => 'Sauf mention contraire, tous les contenus de ce site sont sous licence CC BY-NC-ND.',
			),
			array(
				'option_name' => 'elements_recherche',
				'option_value' => '10',
			),
			array(
				'option_name' => '_masquer_langue',
				'option_value' => 'oui',
			),
			array(
				'option_name' => 'nombre_de_langue',
				'option_value' => '1',
			),
			array(
				'option_name' => 'langue_par_defaut',
				'option_value' => '',
			),
			array(
				'option_name' => 'langue_1',
				'option_value' => 'Français',
			),
			array(
				'option_name' => 'value_langue_1',
				'option_value' => 'fr',
			),
			array(
				'option_name' => 'langue_2',
				'option_value' => '',
			),
			array(
				'option_name' => 'value_langue_2',
				'option_value' => '',
			),
			array(
				'option_name' => 'langue_3',
				'option_value' => '',
			),
			array(
				'option_name' => 'value_langue_3',
				'option_value' => '',
			),
			array(
				'option_name' => 'langue_4',
				'option_value' => '',
			),
			array(
				'option_name' => 'value_langue_4',
				'option_value' => '',
			),
			array(
				'option_name' => 'langue_5',
				'option_value' => '',
			),
			array(
				'option_name' => 'value_langue_5',
				'option_value' => '',
			),
		);

		foreach ($insertion_defaut as $insert) {
			sql_insertq('spip_dsfrconfigurations', $insert);
		}
	}

	return $tables;
}




