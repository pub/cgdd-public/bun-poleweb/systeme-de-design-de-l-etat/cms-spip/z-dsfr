/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2006, 2014 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD (Register as an anonymous module)
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// Node/CommonJS
		module.exports = factory(require('jquery'));
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}

	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}

	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}

	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		try {
			// Replace server-side written pluses with spaces.
			// If we can't decode the cookie, ignore it, it's unusable.
			// If we can't parse the cookie, ignore it, it's unusable.
			s = decodeURIComponent(s.replace(pluses, ' '));
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}

	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}

	var config = $.cookie = function (key, value, options) {

		// Write

		if (arguments.length > 1 && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setMilliseconds(t.getMilliseconds() + days * 864e+5);
			}

			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// Read

		var result = key ? undefined : {},
			// To prevent the for loop in the first place assign an empty array
			// in case there are no cookies at all. Also prevents odd result when
			// calling $.cookie().
			cookies = document.cookie ? document.cookie.split('; ') : [],
			i = 0,
			l = cookies.length;

		for (; i < l; i++) {
			var parts = cookies[i].split('='),
				name = decode(parts.shift()),
				cookie = parts.join('=');

			if (key === name) {
				// If second argument (value) is a function it's a converter...
				result = read(cookie, value);
				break;
			}

			// Prevent storing a cookie that we couldn't decode.
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		// Must not alter options, thus extending a fresh object...
		$.cookie(key, '', $.extend({}, options, { expires: -1 }));
		return !$.cookie(key);
	};

}));

function disableFocus(target) {
    $(target).find('a, button, input').attr('tabindex', -1);
}

function enableFocus(target) {
    $(target).find('a, button, input').attr('tabindex', null);
}

function hide(target) {
    disableFocus(target);
    $(target).attr('aria-hidden', true);
}

function show(target) {
    enableFocus(target);
    $(target).attr('aria-hidden', false);
}


class Init{
    constructor() {
        const html = document.querySelector('html');
        /*let langue_utilisee = $.cookie('langue-site');
        if ($.cookie('langue-site')){            
            $("html").removeAttr("class")
            $("html").addClass("ltr js " + langue_utilisee);
            $("html").attr("xml:lang",langue_utilisee); 
            $("html").attr("lang",langue_utilisee) ;
            
        } else {
            $.cookie('langue-site', 'fr', { expires: 30, path: '/' });                    
        }*/
        $( document ).ready(function() {
            $('a[rel="external"]').addClass('fr-link fr-icon-external-link-fill fr-link--icon-right');
            $("#champ_destinataires_1").addClass("fr-select");
            let ancre = "";
            let url = $(location).attr("href").split('#');
            $('a.spip_note').each(function(i){
                ancre = $(this).attr("href");
                $(this).attr('href',url[0] + ancre);
			});
        });


    

      //determines if the user has a set theme
        function detectColorScheme(){
            var theme="dark";    //default to dark

            //local storage is used to override OS theme settings
            if(localStorage.getItem("scheme")){
                if(localStorage.getItem("scheme") == "dark"){
                    theme = "dark";
                }
                else{
                     theme = "light";
                }
            } else if(!window.matchMedia) {
                //matchMedia method not supported
                 theme = "dark";
                return false;
            } else if(window.matchMedia("(prefers-color-scheme: dark)").matches) {
                //OS theme setting detected as dark
                 theme = "dark";
            }

            //dark theme preferred, set document with a `data-theme` attribute
            
                document.documentElement.setAttribute("data-fr-scheme", theme);
                document.documentElement.setAttribute("data-fr-theme", theme);
        
        }
        detectColorScheme();

    };
}

class Hreftitle{
    constructor() {
		// on ajoute un title à tous les href ayant un rel="external"
        const html = document.querySelector('html');
        $(document).ready(function() {
			$('a[rel="external"]').each(function() {
				if ($.cookie('langue-site')=="en"){
					  var linkText = $(this).text()+" - External link";
				}
				else{
					var linkText = $(this).text()+" - Lien externe";
				}
				$(this).removeAttr('title').attr('title',linkText);
			});
			$('a[rel="noopener external"]').each(function() {
				if ($.cookie('langue-site')=="en"){
					  var linkText = $(this).text()+" - External link";
				}
				else{
					var linkText = $(this).text()+" - Lien externe";
				}
			  $(this).removeAttr('title').attr('title',linkText);
			});
		  });


    };
}


(function() {
    "use strict";

    class Main {
        constructor() {
            const html = document.querySelector('html');
            html.classList.remove('no-js');
            html.classList.add('js');

            const motionQuery = matchMedia('(prefers-reduced-motion)');
            if (motionQuery.matches) {
                html.classList.add('js-reduce-motion');
            }
            
			new Init();
			new Hreftitle();

        }
    }

    const _main = new Main();
}());

