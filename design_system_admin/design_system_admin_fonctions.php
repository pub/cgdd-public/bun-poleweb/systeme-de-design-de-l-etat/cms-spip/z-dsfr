<?php

/**
 * Fonctions utiles au plugin Design system Admin
 *
 * @plugin     Design system Admin
 * @copyright  2023
 * @author     Mikaël Folio
 * @licence    GNU/GPL
 * @package    SPIP\Design_system_admin\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/*
 * Un fichier de fonctions permet de définir des éléments
 * systématiquement chargés lors du calcul des squelettes.
 *
 * Il peut par exemple définir des filtres, critères, balises, …
 *
 */
