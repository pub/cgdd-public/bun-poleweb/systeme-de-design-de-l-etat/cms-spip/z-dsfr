<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = [

	// A
	'ajouter_lien_dsfrconfiguration' => 'Ajouter ce mot',

	// C
	'champ_option_name_label' => 'Option-name',
	'champ_option_value_label' => 'Option-valueDescriptif',
	'confirmer_supprimer_dsfrconfiguration' => 'Confirmez-vous la suppression de cet mot ?',

	// I
	'icone_creer_dsfrconfiguration' => 'Configurer le DSFR',
	'icone_voir_documentation_dsfr' => 'Documentation des modèles DSFR',
	'icone_menu_dsfrconfiguration' => 'Système de design de l\'État',
	'icone_consulter_wiki' => 'Wiki du plugin',
	'icone_gitlab_public' => 'Gitlab public',

];
