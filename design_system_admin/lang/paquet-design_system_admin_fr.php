<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// D
	'design_system_admin_description' => 'Système de design de l\'état (SPIP thème)',
	'design_system_admin_nom' => 'Système de design de l\'état',
	'design_system_admin_slogan' => 'DSFR thème parent',
];
