<?php

/**
 * Fonctions utiles au plugin Design System Child
 *
 * @plugin     design_system_child
 * @copyright  2023
 * @author     Folio Mikaël
 * @licence    GNU/GPL
 * @package    SPIP\design_system_child\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/*
 * Un fichier de fonctions permet de définir des éléments
 * systématiquement chargés lors du calcul des squelettes.
 *
 * Il peut par exemple définir des filtres, critères, balises, …
 *
 */
