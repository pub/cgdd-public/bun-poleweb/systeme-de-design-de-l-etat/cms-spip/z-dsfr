<?php

/**
 * Options au chargement du plugin Design System Child
 *
 * @plugin     Design System Child
 * @copyright  2023
 * @author     Folio Mikaël
 * @licence    GNU/GPL
 * @package    SPIP\design_system_child\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/*
 * Un fichier d'options permet de définir des éléments
 * systématiquement chargés à chaque hit sur SPIP.
 *
 * Il vaut donc mieux limiter au maximum son usage
 * tout comme son volume !
 *
 */
