<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// T
	'design_system_child_description' => 'Thème enfant DSFR pour SPIP, il permet de surcharger le thème principal. Cela évite les problèmes d\'écrasement lors des mise à jour du thème principal.',
	'design_system_child_nom' => 'Système de design de l\'état',
	'design_system_child_slogan' => 'DSFR Thème enfant',
];
