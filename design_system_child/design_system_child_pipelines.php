<?php

/**
 * Utilisations de pipelines par Design System Child
 *
 * @plugin     Design System Child
 * @copyright  2023
 * @author     Folio Mikaël
 * @licence    GNU/GPL
 * @package    SPIP\design_system_child\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/*
 * Un fichier de pipelines permet de regrouper
 * les fonctions de branchement de votre plugin
 * sur des pipelines existants.
 */
