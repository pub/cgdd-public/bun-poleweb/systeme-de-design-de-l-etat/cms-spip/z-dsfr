<BOUCLE_principale(RUBRIQUES) {id_rubrique}>
	[(#REM) Ne pas supprimer la ligne ci-dessous home=yes si page accueil (sommaire.html) home=no pour les autres pages]
	<BOUCLE_descriptif_rubrique(CONDITION){si #DESCRIPTIF|oui}>
		<INCLURE{fond=inclure/head, home=no, titre=#TITRE, descriptif=#DESCRIPTIF, env} />  
	</BOUCLE_descriptif_rubrique>
		<INCLURE{fond=inclure/head, home=no, titre=#TITRE, descriptif=#CONFIG_VALUE{meta_description}, env} />  
	<//B_descriptif_rubrique>
	<INCLURE{fond=inclure/header, home=no, env} />  
	[(#REM) code de votre page sommaire ci-dessous]  
	
	<main role="main" id="contenu">  
        <div class="fr-container-fluid ds_banner" id="contenu">
            <div class="fr-container">	                        	                       
	            <INCLURE{fond=inclure/fil-ariane, env, title=#TITRE, type=rubrique} />
	
	[(#REM) IMPORTANT Ne pas supprimer la partie ci-dessus] 
				 			
				[(#REM) Votre code ci-dessous]
				<section class="bloctitre  fr-mb-4v" >
					<style>
						.darkmode{display:none;width:100%;}
						.lightmode{display:block;width:100%;}
						[data-fr-theme="dark"] .darkmode {display:block;margin-top:-1rem;}
						[data-fr-theme="dark"] .lightmode{display:none;margin-top:-1rem;}
						[data-fr-theme="dark"] .bg_dark_mode{background-color: #6e6e6e;}
						.fr-h4 {
							order: 0;
							margin-bottom: 1rem;
						}
						.fr-card.fr-enlarge-link:not(.fr-card--no-arrow) .fr-card__body > .fr-card__desc, .fr-card.fr-enlarge-link:not(.fr-card--no-arrow) .fr-card__body > .fr-card__title {
							margin-bottom: 1rem;
						}
						.fr-card.fr-enlarge-link:not(.fr-card--no-arrow) .fr-card__body > .fr-card__desc {
							margin-top: 1rem;
						}
						.fr-tags-group > li {
							line-height: initial;
						}
						.bloctitre .motcle:hover {
							background: var(--grey-50-1000);
							color: var(--background-contrast-grey);
						}
						.fr-tile__img {
							width:100%;
							height:100%;
							margin:0;
						}
					</style>
					[(#REM) on affiche ou non la date en fonction du mot clé Afficher date]
					#SET{affichage_date,oui}
					<BOUCLE_mots_tous(MOTS){id_mot=30}{id_rubrique}{par titre}>
						#SET{affichage_date, #TITRE}
					</BOUCLE_mots_tous>
					<div class="[ (#ID_ARTICLE|=={105}?{fr-pb-1v bg_dark_mode,bg_dark_mode})] ">
						<div class="fr-grid-row fr-grid-row--gutters">
							<div class="fr-col-12 [(#GET{affichage_date}|=={oui}|?{fr-col-md-9,fr-col-md-12})]">
								<h1 class="nonaccueil fr-ml-5v">#TITRE</h1>	
							</div>
							<BOUCLE_verifie_affichage(CONDITION){si #GET{affichage_date}|=={oui}|oui}>
								<div class="fr-col-12 fr-col-md-3">
									<p class="fr-tag fr-fi-calendar-line fr-tag--icon-left fr-tag--sm fr-mt-3v">
										<BOUCLE_test_lang_date(CONDITION){si #LANG|=={fr}|oui}>
											#GET{published}&nbsp;[(#DATE|affdate)]
										</BOUCLE_test_lang_date>
											#GET{published}&nbsp;[(#DATE|affdate{F j})], [(#DATE|affdate{Y})]
										<//B_test_lang_date>
									</p>
								</div>
							</BOUCLE_verifie_affichage>
						</div>
					</div>
				</section>
					 
					
				<div class="fr-grid-row fr-grid-row--center fr-grid-row--gutters fr-mb-3v">
					<B_srub>
						<div class="aside fr-col-12 fr-col-md-12 [(#TOTAL_BOUCLE|>={1}|?{fr-col-lg-4, fr-hidden})] ">
							[(#REM) Rubriques dans la meme rubrique ] 
								<div class="fr-sidemenu fr-sidemenu--left ">
									<nav class="fr-sidemenu" role="navigation" aria-label="Menu latéral">
										<div class="fr-sidemenu__inner">
											<button class="fr-sidemenu__btn" hidden aria-controls="fr-sidemenu-wrapper" aria-expanded="true">Dans cette rubrique</button>
											<div class="fr-collapse" id="fr-sidemenu-wrapper">
												<div class="fr-sidemenu__title">Sommaire</div>
												<ul class="fr-sidemenu__list">
													<BOUCLE_srub(RUBRIQUES){id_parent}{par num titre}{doublons}>
													<li class="fr-sidemenu__item #EXPOSE{fr-sidemenu__item--active} ">
														<button class="fr-sidemenu__btn" aria-expanded="false" aria-controls="fr-sidemenu-item-#ID_RUBRIQUE">#TITRE</button>
															<div class="fr-collapse" id="fr-sidemenu-item-#ID_RUBRIQUE">
																<ul class="fr-sidemenu__list">
																	<BOUCLE_dans_srub(ARTICLES){id_rubrique}{par num titre}>
																	<li class="fr-sidemenu__item #EXPOSE{fr-sidemenu__item--active}"><a class="fr-sidemenu__link" href="#URL_ARTICLE" target="_self">#TITRE</a></li>
																	<BOUCLE_recursive(BOUCLE_srub)></BOUCLE_recursive>
																	</BOUCLE_dans_srub>
																</ul>
														</div>		 
													</li>
													</BOUCLE_srub>
												</ul>
											</div>
										
									</nav> 
							</div>
						</div>
					</B_srub>
						
					<div class="fr-col-12 fr-col-md-12 fr-col-lg-8"> 
						<div class="content hentry" id="content">
							<div class="fr-col-12">		
								[<p class="#EDIT{texte} texte surlignable">(#TEXTE)</p>] 					  
							</div>								
						</div>						  
						[(#REM) Articles de la rubrique ]
						
						<B_articles>
							<div class="menu menu_articles actualites-article">#ANCRE_PAGINATION							
								<div class="actus-liste-css ">
									[(#REM) Articles de la rubrique ]
										<div class=" ">
										<BOUCLE_articles(ARTICLES) {id_rubrique} {par num titre}{!par date} {pagination 20}>
											<div class="fr-card fr-enlarge-link fr-card--horizontal fr-card--horizontal-half  fr-card--sm hauteur-limitee fr-mb-5v">
												<div class="fr-card__body fr-background-alt--orange-terre-battue">
													<div class="fr-card__content">
														<h3 class="fr-card__title">
															<a href="#URL_ARTICLE">#TITRE</a>
														</h3>
														<p class="fr-card__desc">[(#INTRODUCTION{250}|textebrut)]</p>

														<div class="fr-card__start">
															
															<B_mots_cles>
																<ul class="fr-tags-group">
																	<BOUCLE_mots_cles(MOTS){id_article}{id_groupe IN 2,3}>
																		<li>
																			<p class="fr-tag">#TITRE</p>
																		</li>
																	</BOUCLE_mots_cles>																				
																</ul>
															</B_mots_cles>
															
															<p class="fr-card__detail  fr-icon-calendar-line">[(#DATE|affdate)]
																
															</p>
														</div>
														[(#REM) div class="fr-card__end">p class="fr-card__detail fr-icon-warning-fill">détail (optionnel)/p>div>]
													</div>
												</div>
												<BOUCLE_actualites(RUBRIQUES){id_rubrique=#ID_RUBRIQUE}{!id_rubrique IN 17,18}>
													<div class="fr-card__header">
														<div class="fr-card__img nomaxheight">
															
																[<div class="fr-tile__img">(#LOGO_ARTICLE_RUBRIQUE|image_reduire{400,*}|?{#LOGO_ARTICLE_RUBRIQUE|image_reduire{400,*},'<object type="image/svg+xml" data="/IMG/svg/icone_actu_light_mode.svg" width="200" height="200"></object>'})</div>]

															
															
														</div>
													</div>
												</BOUCLE_actualites>
											</div>													
										</BOUCLE_articles>
									</div>
									[<p class="pagination fr-mt-3v" style="text-align: center;">(#PAGINATION{page_precedent_suivant})</p>]
								</div>
							</div>
						</B_articles>								 								 
						[<div class="notes"><hr />(#NOTES)</div>]					
			
						<div class="region-highlight fr-col-12">
							<INCLURE{fond=inclure/partage-rs,self=#SELF} />
						</div>
						
					</div>
						
				</div>


[(#REM) IMPORTANT Ne pas supprimer la partie ci-dessous]

			</div>
		</div>
    </main>
	[(#REM) Ne pas supprimer la ligne ci-dessous home=yes si page accueil (sommaire.html) home=no pour les autres pages]      
	<INCLURE{fond=inclure/footer,self=#SELF,  env, home=no} />
</BOUCLE_principale>