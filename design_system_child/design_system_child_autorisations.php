<?php

/**
 * Définit les autorisations du plugin Design System Child
 *
 * @plugin     Design System Child
 * @copyright  2023
 * @author     Folio Mikaël
 * @licence    GNU/GPL
 * @package    SPIP\design_system_child\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/*
 * Un fichier d'autorisations permet de regrouper
 * les fonctions d'autorisations de votre plugin
 */

/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function design_system_child_autoriser() {
}


/* Exemple
function autoriser_design_system_child_configurer_dist($faire, $type, $id, $qui, $opt) {
	// type est un objet (la plupart du temps) ou une chose.
	// autoriser('configurer', '_design_system_child') => $type = 'design_system_child'
	// au choix :
	return autoriser('webmestre', $type, $id, $qui, $opt); // seulement les webmestres
	return autoriser('configurer', '', $id, $qui, $opt); // seulement les administrateurs complets
	return $qui['statut'] == '0minirezo'; // seulement les administrateurs (même les restreints)
	// ...
}
*/
