# Changelog

## 3.1.0 - 2023-04-02

### Added

- #16 : pouvoir vérifier que deux champs ont des valeurs (ou éventuellement des types) différentes
- Compatiblité SPIP 4.2
