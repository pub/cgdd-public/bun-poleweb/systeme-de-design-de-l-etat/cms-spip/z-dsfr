<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-nospam?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'nospam_description' => '¡Deja de mandar spam! Filtra el spam en los formularios sin perjudicar a los usuarios honestos.',
	'nospam_slogan' => 'Limita el riesgo de spams en los foros'
);
