<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-pages?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pages_description' => 'Questo plugin ti consente di creare articoli che non sono collegate a nessuna particolare gerarchia.
Tuttavia, possono essere associati ad un nome template.
Ciò consente di creare pagine di avviso legale, informazioni, contatti, ecc.',
	'pages_slogan' => 'Pagine senza rubrica'
);
