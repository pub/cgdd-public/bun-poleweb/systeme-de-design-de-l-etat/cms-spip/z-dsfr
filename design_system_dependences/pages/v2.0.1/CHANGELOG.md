# Changelog

## 2.0.1 - 2024-01-06

### Fixed

- #12 Correction de l’ergonomie du bouton pour changer l'identifiant de la page

### Added

- Ajout d'un CHANGELOG et d'un README