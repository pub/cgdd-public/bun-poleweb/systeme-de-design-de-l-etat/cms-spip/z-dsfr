<?php
// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/switchcase/trunk/lang/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'switchcase_nom' => 'Balises #SWITCH #CASE et filtre |switchcase',
	'switchcase_slogan' => 'Les balises #SWITCH, #CASE ET #CASE_DEFAULT ainsi que le filtre |switchcase implémentent les contrôles switch... case... default... du php et du C'
);
